<?php
  /* Header */
	get_header();

	/*
		// Check URL and redirect if appropriate.
	
		https://www.ardenhomes.com.au/house/milan/#/floorplans
		https://www.ardenhomes.com.au/house/votivo/#/gallery
		https://www.ardenhomes.com.au/house/votivo/#/floorplans
	*/
	echo "<script>
		var regEx = /(.*)(\/#\/)(floorplan+s?)/g;
		var url = window.location.href;
		if (url.match(regEx) != null) {
			// redirect
			window.location.href = url.replace(regEx, '$1/#$3');
		}
		var regEx = /(.*)(\/#\/)(gallery)/g;
		var url = window.location.href;
		if (url.match(regEx) != null) {
			// redirect
			window.location.href = url.replace(regEx, '$1/#interiors');
		}
	</script>
	";


	/* Home Data */
	$home = Home::FromPage();

	/* Get Category Colour */ 
	$colour = "#425563";
	$category = get_the_category()[0];
	$ranges = get_field('ranges','options');

	if (is_array($ranges)) {
		foreach ($ranges as $range) {
			if ($range['range_name'] == $category->name) {
				$colour = $range['range_colour'];
			}
		}
	}

	echo "
	<style>
		#secondaryNav .navbar .container                      { background-color:{$colour} }
		#secondaryNav .navbar .navbar-nav .nav-item .sub-menu { background-color:{$colour}; }

		.btn-range                                            { color:#FFF; background-color:{$colour}; border-color:{$colour}; }
		.btn-range:hover, .btn-range:focus                    { border-color:#B7A99A; }


	</style>
	";

	echo "<div id=\"home-page\">";

		//  
		//  
		//   ##########   ####    ####  ############  ###########   ####    ####  ############  ############  ####    ####  
		//  ####    ####  ####    ####  ####          ####    ####  ####    ####      ####      ####          ####    ####  
		//  ####    ####  #####  #####  ########      ##########    #####  #####      ####      ########      #### ## ####  
		//  ####    ####    ########    ####          ####    ####    ########        ####      ####          ############  
		//   ##########       ####      ############  ####    ####      ####      ############  ############  #####  #####  
		//  
		//
		echo "<section id=\"home-overview\" class=\"home-tab\">";
			/* Hero Panel */
			$module              = [
				'image'            => $home->hero_image->url,
				'caption'          => $home->hero_caption
			];
			include('module/home/home-hero.php');
			/* Secondary Navigation */
			$home_page           = "overview";
			include('module/home/home-navigation.php');
			/* Features */
			include('module/home/home-facilities.php');
			/* Content Panel */
			$panel_class         = "overview-content";
			$panel_heading       = trim($home->title);
			$panel_content       = $home->description;
			/* Add price */
			if (!is_null($home->price) && !empty($home->price)) {
				$panel_content     = "<p class=\"home-amount\">Priced from {$home->price}</p>".$panel_content;
			}
			$panel_button_class  = null;
			$panel_button        = "";
			$panel_link          = "";
			foreach ($home->floorplan->layouts as $layout) {
				$panel_heading    .= "<span>{$layout->name}</span>";
			}
			include("module/content-panel.php");
			/* Awards */
			include("module/home/home-awards.php");
		echo "</section>";
		//  
		//  
		//  ############    ########      ########      ########    ##########    ############   ###########  
		//  ####          ####    ####  ####    ####  ####    ####  ####    ####  ####          ####          
		//  ########      ############  ####          ############  ####    ####  ########       ##########   
		//  ####          ####    ####  ####    ####  ####    ####  ####    ####  ####                  ####  
		//  ####          ####    ####    ########    ####    ####  ##########    ############  ###########   
		//  
		//  
		echo "<section id=\"home-facades\" class=\"home-tab\">";
			/* Hero Panel */
			$carousel = Carousel::FromOptions('facades');
			$carousel->slides = $home->gallery_facade_slides;
			include('module/carousel/carousel.php');
			/* Secondary Navigation */
			$home_page           = "facades";
			include('module/home/home-navigation.php');
			/* Content Panel */
			$panel_class         = "overview-content";
			$panel_heading       = trim($home->title);
			$panel_content       = $home->description;
			$panel_button_class  = null;
			$panel_button        = "";
			$panel_link          = "";
			foreach ($home->floorplan->layouts as $layout) {
				$panel_heading    .= "<span>{$layout->name}</span>";
			}
			include("module/content-panel.php");
		echo "</section>";
		//  
		//  
		//  ############  #####   ####  ############  ############  ###########   ############   ##########   ###########    ###########  
		//      ####      ######  ####      ####      ####          ####    ####      ####      ####    ####  ####    ####  ####          
		//      ####      #### ## ####      ####      ########      ##########        ####      ####    ####  ##########     ##########   
		//      ####      ####  ######      ####      ####          ####    ####      ####      ####    ####  ####    ####          ####  
		//  ############  ####   #####      ####      ############  ####    ####  ############   ##########   ####    ####  ###########   
		//  
		//
		echo "<section id=\"home-interiors\" class=\"home-tab\">";
			/* Hero Panel */
			$carousel = Carousel::FromOptions('interiors');
			$carousel->slides = $home->gallery_interior_slides;
			include('module/carousel/carousel.php');
			/* Secondary Navigation */
			$home_page           = "interiors";
			include('module/home/home-navigation.php');
			/* Content Panel */
			$panel_class         = "overview-content";
			$panel_heading       = trim($home->title);
			$panel_content       = $home->description;
			$panel_button_class  = null;
			$panel_button        = "";
			$panel_link          = "";
			foreach ($home->floorplan->layouts as $layout) {
				$panel_heading    .= "<span>".trim($layout->name)."</span>";
			}
			include("module/content-panel.php");
		echo "</section>";
		//  
		//  
		//  ###########   ####            ########    #####   ####   ###########  
		//  ####    ####  ####          ####    ####  ######  ####  ####          
		//  ###########   ####          ############  #### ## ####   ##########   
		//  ####          ####          ####    ####  ####  ######          ####  
		//  ####          ############  ####    ####  ####   #####  ###########   
		//  
		//
		foreach ($home->floorplan->layouts as $layout) {
			$home_page = strtolower(trim($home->title));
			if (!empty($layout->name)) {
				$home_page .= "-".strtolower(str_replace(" ", "-", trim($layout->name)));
			}

			echo "
				<section id=\"home-{$home_page}\" class=\"home-tab home-floorplan\">
			";
				/* Hero Panel */
				$carousel = Carousel::FromOptions($home_page);
				$carousel->slides = $layout->levels;
				include('module/carousel/carousel.php');

				/* Secondary Navigation */
				include('module/home/home-navigation.php');

				echo "
					<div class=\"container home-dimensions\">
						<div class=\"row\">
				";
				/* Show selected home */
				$layout_data = $layout;
				$wrap_class = "";
				$column = 0;
				include('module/home/home-dimension.php');
				/* Show other homes */
				foreach ($home->floorplan->layouts as $others) {
					if ($layout->name != $others->name) {
						$layout_data = $others;
						$wrap_class = ($column++%2==0)?" alt-dimension":"";
						include('module/home/home-dimension.php');
					}
				}

				$layout_name = str_replace(" ", "-", $layout->name);

				echo "
						</div>
					</div>
				</section>

				<div class=\"modal modal-image-block\" id=\"modal_".trim($home->title)."_{$layout_name}\">
				  <div class=\"modal-dialog modal-sm\">
				    <div class=\"modal-content\">
				      <div class=\"modal-header\">
				        <h4 class=\"modal-title\">".trim($home->title)." {$layout_name} Floorplan Options</h4>
				        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
				      </div>
				      <div class=\"modal-body\">
				";

				// print_r($home->floorplan->options);
				$carousel = Carousel::FromOptions("carousel_".trim($home->title)."_{$layout_name}");
				$carousel->slides = $layout->options;
				$carousel->height = null;
				$carousel->bgcolour = "#FFF";
				include('module/carousel/carousel-images.php');

				echo "
				      </div>
				    </div>
				  </div>
				</div>
				";



		}
		//  
		//  
		//  ############  #####   ####    ########    ####          ####    ####   ###########  ############   ##########   #####   ####   ###########  
		//      ####      ######  ####  ####    ####  ####          ####    ####  ####              ####      ####    ####  ######  ####  ####          
		//      ####      #### ## ####  ####          ####          ####    ####   ##########       ####      ####    ####  #### ## ####   ##########   
		//      ####      ####  ######  ####    ####  ####          ####    ####          ####      ####      ####    ####  ####  ######          ####  
		//  ############  ####   #####    ########    ############    ########    ###########   ############   ##########   ####   #####  ###########   
		//  
		//
		echo "<section id=\"home-inclusions\" class=\"home-tab\">";
			/* Hero Panel */
			$module              = [
				'image'            => $home->inclusion_image->url,
				'caption'          => $home->inclusion_caption
			];
			include('module/home/home-hero.php');
			/* Secondary Navigation */
			$home_page = "inclusions";
			include('module/home/home-navigation.php');
			/* Content Panel */
			$panel_class         = "inclusion-content";
			$panel_heading       = $home->inclusion_heading;
			$panel_content       = $home->inclusion_description;
			$panel_button_class  = "btn-range";
			$panel_button        = $home->inclusion_button_text;
			$panel_link          = trim($home->inclusion_file['url']);
			include("module/content-panel.php");
		echo "</section>";
		//  
		//  
		//  ###########   ###########    ##########   ####    ####   ##########   ############  ############   ##########   #####   ####  
		//  ####    ####  ####    ####  ####    ####  #####  #####  ####    ####      ####          ####      ####    ####  ######  ####  
		//  ###########   ##########    ####    ####  ############  ####    ####      ####          ####      ####    ####  #### ## ####  
		//  ####          ####    ####  ####    ####  #### ## ####  ####    ####      ####          ####      ####    ####  ####  ######  
		//  ####          ####    ####   ##########   ####    ####   ##########       ####      ############   ##########   ####   #####  
		//  
		// 
		echo "<section id=\"home-promotion\" class=\"home-tab\">";
			/* Hero Panel */
			$module              = [
				'image'            => $home->promotion_image->url,
				'caption'          => $home->promotion_caption
			];
			include('module/home/home-hero.php');
			/* Secondary Navigation */
			$home_page           = "promotion";
			include('module/home/home-navigation.php');
			/* Content Panel */
			$panel_class         = "promotion-content";
			$panel_heading       = $home->promotion_heading;
			$panel_content       = $home->promotion_description;
			$panel_button_class  = "btn-tertiary-blank";
			$panel_button        = $home->promotion_button_text;
			$panel_link          = trim($home->promotion_file['url']);
			include("module/content-panel.php");
		echo "</section>";
		//
		// -- END ---------------------------------------------

?>
		</div>
	</div>
<?php
	// Footer
 	get_footer();

?>
