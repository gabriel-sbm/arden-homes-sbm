<?php /** Custom Post Type **/
	add_action( 'init', 'create_post_type' );
	function create_post_type() {

		// HOME
		//
		register_post_type( 'home',
			[
				'labels'          => [
					'name'          => __( 'Homes' ),
					'singular_name' => __( 'Home' ),
					'add_new'       => _x( 'Add New', 'Home' ),
					'add_new_item'  => __( 'Add New Home' ),
					'edit_item'     => __( 'Edit Home' ),
					'new_item'      => __( 'New Home' ),
					'all_items'     => __( 'All Homes' )
				],
				'public'          => true,
				'has_archive'     => true,
				'menu_icon'       => 'dashicons-admin-home',
				'taxonomies'      => [ 'category' ],
				'supports'        => [ 'title', 'thumbnail' ],
				'rewrite'         => [ 'slug' => 'house', 'with_front' => false]
			]
		);

		// Awards
		//
		register_post_type( 'award',
			[
				'labels'          => [
					'name'          => __( 'Awards' ),
					'singular_name' => __( 'Award' ),
					'add_new'       => _x( 'Add New', 'Award' ),
					'add_new_item'  => __( 'Add New Award' ),
					'edit_item'     => __( 'Edit Award' ),
					'new_item'      => __( 'New Award' ),
					'all_items'     => __( 'All Awards' )
				],
				'public'          => true,
				'has_archive'     => true,
				'menu_icon'       => 'dashicons-welcome-learn-more',
				'taxonomies'      => [ 'category' ],
				'supports'        => [ 'title', 'thumbnail' ]
			]
		);

		// Awards
		//
		register_post_type( 'resource',
			[
				'labels'          => [
					'name'          => __( 'Resources' ),
					'singular_name' => __( 'Resource' ),
					'add_new'       => _x( 'Add New', 'Resource' ),
					'add_new_item'  => __( 'Add New Resource' ),
					'edit_item'     => __( 'Edit Resource' ),
					'new_item'      => __( 'New Resource' ),
					'all_items'     => __( 'All Resources' )
				],
				'public'          => true,
				'has_archive'     => true,
				'menu_icon'       => 'dashicons-welcome-widgets-menus',
				'taxonomies'      => [ 'category' ],
				'supports'        => [ 'title', 'editor', 'thumbnail' ]
			]
		);
		if (function_exists('acf_add_options_page')) {
		  acf_add_options_page(array(
		    'page_title'  => 'Settings',
		    'menu_slug' => 'resource-settings',
		    'parent_slug' => 'edit.php?post_type=resource',
		  ));
		}

		// // SERVICES
		// //
		// register_post_type( 'service',
		// 	[
		// 		'labels'          => [
		// 			'name'          => __( 'Services' ),
		// 			'singular_name' => __( 'Service' ),
		// 			'add_new'       => _x( 'Add New', 'Service' ),
		// 			'add_new_item'  => __( 'Add New Service' ),
		// 			'edit_item'     => __( 'Edit Service' ),
		// 			'new_item'      => __( 'New Service' ),
		// 			'all_items'     => __( 'All Services' )
		// 		],
		// 		'public'          => true,
		// 		'has_archive'     => true,
		// 		'menu_icon'       => 'dashicons-admin-tools',
 		// 		'taxonomies'      => [ 'category' ],
		// 		'supports'        => [ 'title', 'thumbnail' ]
		// 	]
		// );

		// // PRODUCTS
		// //
		// register_post_type( 'product',
		// 	[
		// 		'labels'          => [
		// 			'name'          => __( 'Products' ),
		// 			'singular_name' => __( 'Product' ),
		// 			'add_new'       => _x( 'Add New', 'Product' ),
		// 			'add_new_item'  => __( 'Add New Product' ),
		// 			'edit_item'     => __( 'Edit Product' ),
		// 			'new_item'      => __( 'New Product' ),
		// 			'all_items'     => __( 'All Products' )
		// 		],
		// 		'public'          => true,
		// 		'has_archive'     => false,
		// 		'menu_icon'       => 'dashicons-admin-generic',
		// 		'supports'        => [ 'title', 'thumbnail' ]
		// 	]
		// );


	}
?>