<?php
/*
		Advanced Custom Fields
		- HOME PAGE TEMPLATE

*/
	if(function_exists("register_field_group")) {
		acf_add_local_field_group(array(
			'key' => 'group_5b3464ebd3e72',
			'title' => 'Home',
			'fields' => array(
				//
				// Testimonial Settings
				//
				array(
					'key' => 'home_testimonial_panel_tab',
					'label' => 'Testimonial Settings',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'testimonials-slide-panel-title-tag',
					'label' => 'Title Style',
					'name' => 'testimonials_slide-panel_title_tag',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'module_title' => 'Module title',
						'h1' => 'Header 1',
						'h2' => 'Header 2',
						'h3' => 'Header 3',
						'h4' => 'Header 4',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'testimonials-slide-panel-title',
					'label' => 'Title',
					'name' => 'testimonials_slide-panel_title',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'testimonials-slide-panel-content',
					'label' => 'Content',
					'name' => 'testimonials_slide-panel_content',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'testimonials-slide-panel-width',
					'label' => 'Module Width',
					'name' => 'testimonials_slide-panel_module_width',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'default' => 'Container',
						'full-width' => 'Full Width',
					),
					'default_value' => 'full-width',
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'testimonials-slide-panel-position',
					'label' => 'Content Position',
					'name' => 'testimonials_slide-panel_position',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'panel-left' => 'Left',
						'panel-centre' => 'Centre',
						'panel-right' => 'Right',
					),
					'default_value' => 'panel-centre',
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'testimonials-slide-panel-background',
					'label' => 'Content Background',
					'name' => 'testimonials_slide-panel_background',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'no-bg' => 'No Colour',
						'grad_primary_lr' => 'Gradient Primary - Left to Right',
						'grad_primary_rl' => 'Gradient Primary - Right to Left',
						'grad_secondary_lr' => 'Gradient Secondary - Left to Right',
						'grad_secondary_rl' => 'Gradient Secondary - Right to Left',
						'bg_primary' => 'Solid Background - Primary Colour',
						'bg_secondary' => 'Solid Background - Secondary Colour',
						'bg_tertiary' => 'Solid Background - Tertiary Colour',
						'solid' => 'Solid Colour',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'testimonials-slide-panel-colour',
					'label' => 'Solid Colour',
					'name' => 'testimonials_slide-panel_colour',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'testimonials-slide-panel-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#F1F1F1',
				),
				//
				// Testimonial Slides
				//
				array(
					'key' => 'home_testimonial_slides_tab',
					'label' => 'Testimonial Slides',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'home_testimonials',
					'label' => 'Testimonial Slides',
					'name' => 'home_testimonials',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'testimony_slide_heading',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => 'Add testimony',
					'sub_fields' => array(
						array(
							'key' => 'testimony_slide_heading',
							'label' => 'Author',
							'name' => 'testimony_slide_heading',
							'type' => 'text',
							'instructions' => 'This is the name that appears under the testimony.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array(
							'key' => 'testimony_slide_link',
							'label' => 'Location',
							'name' => 'testimony_slide_link',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array(
							'key' => 'testimony_slide_content',
							'label' => 'Testimony',
							'name' => 'testimony_slide_content',
							'type' => 'wysiwyg',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 1,
							'delay' => 0,
						),
					),
				),
				//
				// Panel links - Feature Group (fg)
				//
				array(
					'key' => 'panel-links-fg-tab',
					'label' => 'Panel links - Feature Group',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'panel-links-fg-data-source',
					'label' => 'Data Source',
					'name' => 'panel-links_fg_data_source',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'post' => 'News',
						'service' => 'Services',
						'product' => 'Products',
						'project' => 'Projects',
						'custom'  => 'Custom',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'panel-links-fg-custom-data',
					'label' => 'Custom Data',
					'name' => 'panel-links_fg_custom_data',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'panel-links-fg-data-source',
								'operator' => '==',
								'value' => 'custom',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'panel-links-fg-data-title',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => 'Add Feature',
					'sub_fields' => array(
						array(
							'key' => 'panel-links-fg-data-images-only',
							'label' => 'Images Only',
							'name' => 'panel-links_fg_data_images_only',
							'type' => 'true_false',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'message' => '',
							'default_value' => 1,
							'ui' => 0,
							'ui_on_text' => '',
							'ui_off_text' => '',
						),
						array(
							'key' => 'panel-links-fg-data-title',
							'label' => 'Title',
							'name' => 'panel-links_fg_data_title',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'panel-links-fg-data-images-only',
										'operator' => '==',
										'value' => '0',
									),
								),
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array(
							'key' => 'panel-links-fg-data-content',
							'label' => 'Description',
							'name' => 'panel-links_fg_data_content',
							'type' => 'textarea',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'panel-links-fg-data-images-only',
										'operator' => '==',
										'value' => '0',
									),
								),
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'maxlength' => '',
							'rows' => '',
							'new_lines' => '',
						),
						array(
							'key' => 'panel-links-fg-data-image',
							'label' => 'Image',
							'name' => 'panel-links_fg_data_image',
							'type' => 'image',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => '',
							'max_height' => '',
							'max_size' => '',
							'mime_types' => '',
						),
						array(
							'key' => 'panel-links-fg-data-button-text',
							'label' => 'Button Text',
							'name' => 'panel-links_fg_data_button_text',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'panel-links-fg-data-images-only',
										'operator' => '==',
										'value' => '0',
									),
								),
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array(
							'key' => 'panel-links-fg-data-button-link',
							'label' => 'Button Link',
							'name' => 'panel-links_fg_data_button_link',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'panel-links-fg-data-images-only',
										'operator' => '==',
										'value' => '0',
									),
								),
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
					),
				),
				array(
					'key' => 'panel-links-fg-is-carousel',
					'label' => 'Use Carousel',
					'name' => 'panel-links_fg_is_carousel',
					'type' => 'checkbox',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'yes' => 'Yes',
					),
					'allow_custom' => 0,
					'save_custom' => 0,
					'default_value' => array(
					),
					'layout' => 'vertical',
					'toggle' => 0,
					'return_format' => 'value',
				),
				array(
					'key' => 'panel-links-fg-max-features',
					'label' => 'Max Features',
					'name' => 'panel-links_fg_max_features',
					'type' => 'text',
					'instructions' => 'What is the maximum number of features to display. Setting this to -1 will display all.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => -1,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'panel-links-fg-feature-style',
					'label' => 'Feature Style',
					'name' => 'panel-links_fg_feature_style',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'title-content-button' => 'Title, content & button',
						'title-background' => 'Title & background',
						'alternating-panel' => 'Alternating Panel Strips',
						'post-background' => 'Post with all info and background',
						'background' => 'Background image',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'panel-links-fg-title-tag',
					'label' => 'Title Style',
					'name' => 'panel-links_fg_title_tag',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'module_title' => 'Module title',
						'h1' => 'Header 1',
						'h2' => 'Header 2',
						'h3' => 'Header 3',
						'h4' => 'Header 4',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'panel-links-fg-title',
					'label' => 'Title',
					'name' => 'panel-links_fg_title',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => 'br',
				),
				array(
					'key' => 'panel-links-fg-content',
					'label' => 'Content',
					'name' => 'panel-links_fg_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array(
					'key' => 'panel-links-fg-width',
					'label' => 'Module Width',
					'name' => 'panel-links_fg_module_width',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'default' => 'Container',
						'full-width' => 'Full Width',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'panel-links-fg-image',
					'label' => 'Background Image',
					'name' => 'panel-links_fg_image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array(
					'key' => 'panel-links-fg-width-content',
					'label' => 'Content Width',
					'name' => 'panel-links_fg_module_width_content',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'default' => 'Container',
						'full-width' => 'Full Width',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'panel-links-fg-position',
					'label' => 'Content Position',
					'name' => 'panel-links_fg_position',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'es-left' => 'Left',
						'es-right' => 'Right',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'panel-links-fg-background',
					'label' => 'Content Background',
					'name' => 'panel-links_fg_background',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'no-bg' => 'No Colour',
						'grad_primary_lr' => 'Gradient Primary - Left to Right',
						'grad_primary_rl' => 'Gradient Primary - Right to Left',
						'grad_secondary_lr' => 'Gradient Secondary - Left to Right',
						'grad_secondary_rl' => 'Gradient Secondary - Right to Left',
						'bg_primary' => 'Solid Background - Primary Colour',
						'bg_secondary' => 'Solid Background - Secondary Colour',
						'bg_tertiary' => 'Solid Background - Tertiary Colour',
						'solid' => 'Solid Colour',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'panel-links-fg-font',
					'label' => 'Font Colour',
					'name' => 'panel-links_fg_font',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'panel-links-fg-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#333',
				),
				array(
					'key' => 'panel-links-fg-colour',
					'label' => 'Solid Colour',
					'name' => 'panel-links_fg_colour',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'panel-links-fg-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#F1F1F1',
				),
				//
				// Action Panel - Call To Action
				//
				array(
					'key' => 'action-panel-cta-tab',
					'label' => 'Action Panel - CTA',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'action-panel-cta-title-tag',
					'label' => 'Title Style',
					'name' => 'action-panel_cta_title_tag',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'module_title' => 'Module title',
						'h1' => 'Header 1',
						'h2' => 'Header 2',
						'h3' => 'Header 3',
						'h4' => 'Header 4',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'action-panel-cta-title',
					'label' => 'Title',
					'name' => 'action-panel_cta_title',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'action-panel-cta-content',
					'label' => 'Content',
					'name' => 'action-panel_cta_content',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'action-panel-cta-button-text',
					'label' => 'Button Text',
					'name' => 'action-panel_cta_button_text',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'action-panel-cta-button-link',
					'label' => 'Button Link',
					'name' => 'action-panel_cta_button_link',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'action-panel-cta-button-style',
					'label' => 'Button Style',
					'name' => 'action-panel_cta_button_style',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'btn' => 'Default',
						'btn btn-primary' => 'Primary',
						'btn btn-secondary' => 'Secondary',
						'btn btn-tertiary' => 'Tertiary',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'action-panel-cta-width',
					'label' => 'Module Width',
					'name' => 'action-panel_cta_module_width',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'default' => 'Container',
						'full-width' => 'Full Width',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'action-panel-cta-position',
					'label' => 'Content Position',
					'name' => 'action-panel_cta_position',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'es-left' => 'Left',
						'es-centre' => 'Centre',
						'es-right' => 'Right',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'action-panel-cta-background',
					'label' => 'Content Background',
					'name' => 'action-panel_cta_background',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'no-bg' => 'No Colour',
						'grad_primary_lr' => 'Gradient Primary - Left to Right',
						'grad_primary_rl' => 'Gradient Primary - Right to Left',
						'grad_secondary_lr' => 'Gradient Secondary - Left to Right',
						'grad_secondary_rl' => 'Gradient Secondary - Right to Left',
						'bg_primary' => 'Solid Background - Primary Colour',
						'bg_secondary' => 'Solid Background - Secondary Colour',
						'bg_tertiary' => 'Solid Background - Tertiary Colour',
						'solid' => 'Solid Colour',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'action-panel-cta-colour',
					'label' => 'Solid Colour',
					'name' => 'action-panel_cta_colour',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'action-panel-cta-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#F1F1F1',
				),
				//
				// Footer 
				//
				array(
					'key' => 'footer-tab',
					'label' => 'Footer',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' =>  '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'footer-separator',
					'label' => 'Separate Footer',
					'name' => 'footer_separator',
					'type' => 'true_false',
					'instructions' => 'This will show a separator line before the footer starts, generally the Stay Updated section.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 1,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),


				// End -----------------
			),
			'location' => array(
				array(
					array(
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'page-home.php',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => array(
				0 => 'the_content',
				1 => 'featured_image',
			),
			'active' => 1,
			'description' => '',
		));

	}
?>