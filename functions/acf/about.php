<?php
/*
		Advanced Custom Fields
		- ABOUT PAGE TEMPLATE

*/
	if(function_exists("register_field_group")) {
		acf_add_local_field_group(array(
			'key' => 'group_about_page',
			'title' => 'About',
			'fields' => array(

				//
				// Carousel Panel - Carousel
				//
				array(
					'key' => 'ah-carousel-panel-settings',
					'label' => 'Hero Carousel Panel Settings',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'ah-carousel-panel-carousel_height',
					'label' => 'Carousel Height',
					'name' => 'ah-carousel-panel_carousel_height',
					'type' => 'number',
					'instructions' => 'Measurement in pixels.',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => 800,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array(
					'key' => 'ah-carousel-panel-animation_style',
					'label' => 'Animation Style',
					'name' => 'ah-carousel-panel_animation_style',
					'type' => 'radio',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'slide' => 'Slide',
						'fader' => 'Fade',
					),
					'allow_null' => 0,
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'slide',
					'layout' => 'horizontal',
					'return_format' => 'value',
				),
				array(
					'key' => 'ah-carousel-panel-animation_delay',
					'label' => 'Animation Delay',
					'name' => 'ah-carousel-panel_animation_delay',
					'type' => 'range',
					'instructions' => 'This is the delay between changing slides.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => 4,
					'min' => 1,
					'max' => 20,
					'step' => '',
					'prepend' => '',
					'append' => '',
				),
				array(
					'key' => 'ah-carousel-panel-carousel_background_colour',
					'label' => 'Carousel Background Colour',
					'name' => 'ah-carousel-panel_carousel_background_colour',
					'type' => 'color_picker',
					'instructions' => 'Seen on first load and between fading slides.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#767676',
				),
				array(
					'key' => 'ah-carousel-panel-slides_tab',
					'label' => 'Hero Carousel Panel Slides',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'ah-carousel-panel-slides',
					'label' => 'Slides',
					'name' => 'ah-carousel-panel_slides',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'ah-carousel-panel-slide_image',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => 'Add slide',
					'sub_fields' => array(
						array(
								'key' => 'ah-carousel-panel-slide_heading',
								'label' => 'Caption',
								'name' => 'ah-carousel-panel_slide_heading',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array(
								'key' => 'ah-carousel-panel-slide_content',
								'label' => 'Content',
								'name' => 'ah-carousel-panel_slide_content',
								'type' => 'wysiwyg',
								'instructions' => 'Optional',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'tabs' => 'all',
								'toolbar' => 'full',
								'media_upload' => 1,
								'delay' => 0,
							),
							array(
									'key' => 'ah-carousel-panel-slide_image',
									'label' => 'Image',
									'name' => 'ah-carousel-panel_slide_image',
									'type' => 'image',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '50%',
										'class' => '',
										'id' => '',
									),
									'return_format' => 'array',
									'preview_size' => 'thumbnail',
									'library' => 'all',
									'min_width' => '',
									'min_height' => '',
									'min_size' => '',
									'max_width' => '',
									'max_height' => '',
									'max_size' => 1,
									'mime_types' => '',
								),
							array(
								'key' => 'ah-carousel-panel-slide_image_alignment',
								'label' => 'Image Alignment',
								'name' => 'ah-carousel-panel_slide_image_alignment',
								'type' => 'select',
								'instructions' => 'Horisontal position followed by vertical position.',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '50%',
									'class' => '',
									'id' => '',
							),
							'choices' => array(
								'left top' => 'Left, Top',
								'center top' => 'Centre, Top',
								'right top' => 'Right, Top',
								'left center' => 'Left, Centre',
								'center' => 'Centre, Centre',
								'right center' => 'Right, Centre',
								'left bottom' => 'Left, Bottom',
								'center bottom' => 'Centre, Bottom',
								'right bottom' => 'Right, Bottom',
							),
							'default_value' => array(
								0 => 'center',
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 0,
							'ajax' => 0,
							'return_format' => 'value',
							'placeholder' => '',
						),
					),
				
				),
				array(
					'key' => 'ah-carousel-panel-use_placeholders',
					'label' => 'Use Placeholders',
					'name' => 'ah-carousel-panel_use_placeholders',
					'type' => 'checkbox',
					'instructions' => 'Use background placeholders if no slides are available.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'yes' => 'Yes',
					),
					'allow_custom' => 0,
					'save_custom' => 0,
					'default_value' => array(
					),
					'layout' => 'vertical',
					'toggle' => 0,
					'return_format' => 'value',
				),
				array(
					'key' => 'ah-carousel-panel-placeholders',
					'label' => 'Placeholders',
					'name' => 'ah-carousel-panel_placeholders',
					'type' => 'repeater',
					'instructions' => 'If no placeholder images are avaiable, default placeholder images will be generated.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => '',
					'sub_fields' => array(
						array(
							'key' => 'ah-carousel-panel-image_urls',
							'label' => 'Image Urls',
							'name' => 'ah-carousel-panel_image_urls',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
					),
				),
				//
				// CONTENT - FIRST BLOCK
				//
				array(
					'key' => 'about-page-content-first-tab',
					'label' => 'Page Content - First Block',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'about-page-heading',
					'label' => 'Page Heading',
					'name' => 'about_page_heading',
					'type' => 'text',
					'instructions' => 'This is the main heading for the page (H1).',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array(
					'key' => 'about-page-introduction',
					'label' => 'Introduction',
					'name' => 'about_page_introduction',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array(
					'key' => 'about-page-content',
					'label' => 'Content',
					'name' => 'about_page_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				//
				// Carousel Panel - Carousel
				//
				array(
					'key' => 'ac-carousel-panel-settings',
					'label' => 'Carousel Panel Settings',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'ac-carousel-panel-carousel_height',
					'label' => 'Carousel Height',
					'name' => 'ac-carousel-panel_carousel_height',
					'type' => 'number',
					'instructions' => 'Measurement in pixels.',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => 800,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array(
					'key' => 'ac-carousel-panel-animation_style',
					'label' => 'Animation Style',
					'name' => 'ac-carousel-panel_animation_style',
					'type' => 'radio',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'slide' => 'Slide',
						'fader' => 'Fade',
					),
					'allow_null' => 0,
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'slide',
					'layout' => 'horizontal',
					'return_format' => 'value',
				),
				array(
					'key' => 'ac-carousel-panel-animation_delay',
					'label' => 'Animation Delay',
					'name' => 'ac-carousel-panel_animation_delay',
					'type' => 'range',
					'instructions' => 'This is the delay between changing slides.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => 4,
					'min' => 1,
					'max' => 20,
					'step' => '',
					'prepend' => '',
					'append' => '',
				),
				array(
					'key' => 'ac-carousel-panel-carousel_background_colour',
					'label' => 'Carousel Background Colour',
					'name' => 'ac-carousel-panel_carousel_background_colour',
					'type' => 'color_picker',
					'instructions' => 'Seen on first load and between fading slides.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#767676',
				),
				array(
					'key' => 'ac-carousel-panel-slides_tab',
					'label' => 'Carousel Panel Slides',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'ac-carousel-panel-slides',
					'label' => 'Slides',
					'name' => 'ac-carousel-panel_slides',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'ac-carousel-panel-slide_image',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => 'Add slide',
					'sub_fields' => array(
						array(
								'key' => 'ac-carousel-panel-slide_heading',
								'label' => 'Caption',
								'name' => 'ac-carousel-panel_slide_heading',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array(
								'key' => 'ac-carousel-panel-slide_content',
								'label' => 'Content',
								'name' => 'ac-carousel-panel_slide_content',
								'type' => 'wysiwyg',
								'instructions' => 'Optional',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'tabs' => 'all',
								'toolbar' => 'full',
								'media_upload' => 1,
								'delay' => 0,
							),
							array(
									'key' => 'ac-carousel-panel-slide_image',
									'label' => 'Image',
									'name' => 'ac-carousel-panel_slide_image',
									'type' => 'image',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '50%',
										'class' => '',
										'id' => '',
									),
									'return_format' => 'array',
									'preview_size' => 'thumbnail',
									'library' => 'all',
									'min_width' => '',
									'min_height' => '',
									'min_size' => '',
									'max_width' => '',
									'max_height' => '',
									'max_size' => 1,
									'mime_types' => '',
								),
							array(
								'key' => 'ac-carousel-panel-slide_image_alignment',
								'label' => 'Image Alignment',
								'name' => 'ac-carousel-panel_slide_image_alignment',
								'type' => 'select',
								'instructions' => 'Horisontal position followed by vertical position.',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '50%',
									'class' => '',
									'id' => '',
							),
							'choices' => array(
								'left top' => 'Left, Top',
								'center top' => 'Centre, Top',
								'right top' => 'Right, Top',
								'left center' => 'Left, Centre',
								'center' => 'Centre, Centre',
								'right center' => 'Right, Centre',
								'left bottom' => 'Left, Bottom',
								'center bottom' => 'Centre, Bottom',
								'right bottom' => 'Right, Bottom',
							),
							'default_value' => array(
								0 => 'center',
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 0,
							'ajax' => 0,
							'return_format' => 'value',
							'placeholder' => '',
						),
					),
				
				),
				array(
					'key' => 'ac-carousel-panel-use_placeholders',
					'label' => 'Use Placeholders',
					'name' => 'ac-carousel-panel_use_placeholders',
					'type' => 'checkbox',
					'instructions' => 'Use background placeholders if no slides are available.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'yes' => 'Yes',
					),
					'allow_custom' => 0,
					'save_custom' => 0,
					'default_value' => array(
					),
					'layout' => 'vertical',
					'toggle' => 0,
					'return_format' => 'value',
				),
				array(
					'key' => 'ac-carousel-panel-placeholders',
					'label' => 'Placeholders',
					'name' => 'ac-carousel-panel_placeholders',
					'type' => 'repeater',
					'instructions' => 'If no placeholder images are avaiable, default placeholder images will be generated.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => '',
					'sub_fields' => array(
						array(
							'key' => 'ac-carousel-panel-image_urls',
							'label' => 'Image Urls',
							'name' => 'ac-carousel-panel_image_urls',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
					),
				),
				//
				// CONTENT - SECOND BLOCK
				//
				array(
					'key' => 'about-page-content-second-tab',
					'label' => 'Page Content - Second Block',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'about-second-page-heading',
					'label' => 'Page Heading',
					'name' => 'about-second_page_heading',
					'type' => 'text',
					'instructions' => 'This is a secondary heading (H2).',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array(
					'key' => 'about-second-page-content',
					'label' => 'Content',
					'name' => 'about-second_page_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),

				//
				// Footer 
				//
				array(
					'key' => 'footer-tab',
					'label' => 'Footer',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' =>  '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'footer-separator',
					'label' => 'Separate Footer',
					'name' => 'footer_separator',
					'type' => 'true_false',
					'instructions' => 'This will show a separator line before the footer starts, generally the Stay Updated section.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 1,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),

				// End -----------------
			),
			'location' => array(
				array(
					array(
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'page-about.php',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => array(
				0 => 'the_content',
				1 => 'featured_image',
			),
			'active' => 1,
			'description' => '',
		));

	}
?>