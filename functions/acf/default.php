<?php
/*
		Advanced Custom Fields
		- DEFAULT PAGE TEMPLATE

*/
	if(function_exists("register_field_group")) {
		//
		// CONTENT
		//
		acf_add_local_field_group(array(
			'key' => 'group_default_content',
			'title' => 'Content',
			'fields' => array(

				// Hero
				array(
					'key' => 'default-hero-tab',
					'label' => 'Hero',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'default-hero-caption',
					'label' => 'Hero Caption',
					'name' => 'default_hero_caption',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array(
					'key' => 'default-hero-content',
					'label' => 'Hero Content',
					'name' => 'default_hero_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => 4,
					'new_lines' => 'br',
				),
				array(
					'key' => 'default-hero-image',
					'label' => 'Hero Image',
					'name' => 'default_hero_image',
					'type' => 'image',
					'instructions' => 'If left empty, the hero image will not be shown.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array(
					'key' => 'default-hero-internal-link',
					'label' => 'Hero Internal Link',
					'name' => 'default_hero_internal_link',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
				array(
					'key' => 'default-hero-page-link',
					'label' => 'Hero Page Link',
					'name' => 'default_hero_page_link',
					'type' => 'page_link',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'default-hero-internal-link',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => '',
					'taxonomy' => '',
					'allow_null' => 0,
					'allow_archives' => 1,
					'multiple' => 0,
				),
				array(
					'key' => 'default-hero-external-link',
					'label' => 'Hero External Link',
					'name' => 'default_hero_external_link',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'default-hero-internal-link',
								'operator' => '!=',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),


				array(
					'key' => 'default-content-tab',
					'label' => 'Page Content',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'default-hide-h1',
					'label' => 'Hide H1 Heading',
					'name' => 'default_hide_h1',
					'type' => 'true_false',
					'instructions' => 'Use this if the H1 is only to be visible to screen readers.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
				array(
					'key' => 'default-page-introduction',
					'label' => 'Introduction',
					'name' => 'default_introduction',
					'type' => 'wysiwyg',
					'instructions' => 'Use this for the intro paragraph which has a larger font size.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array(
					'key' => 'default-page-content',
					'label' => 'Content',
					'name' => 'default_content',
					'type' => 'wysiwyg',
					'instructions' => 'Use this for standard page content.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				//
				// Footer 
				//
				array(
					'key' => 'footer-tab',
					'label' => 'Footer',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' =>  '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'footer-separator',
					'label' => 'Separate Footer',
					'name' => 'footer_separator',
					'type' => 'true_false',
					'instructions' => 'This will show a separator line before the footer starts, generally the Stay Updated section.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 1,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),


				// End -----------------
			),
			'location' => array(
				array(
					array(
						'param' => 'post_template',
						'operator' => '==',
						'value' => 'default',
					),
				),
				array(
					array(
						'param' => 'post_template',
						'operator' => '==',
						'value' => 'page-employment.php',
					),
				),
				array(
					array(
						'param' => 'post_template',
						'operator' => '==',
						'value' => 'page-small.php',
					),
				),
				array(
					array(
						'param' => 'post_template',
						'operator' => '==',
						'value' => 'page-normal.php',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => array(
				0 => 'the_content',
				1 => 'featured_image',
			),
			'active' => 1,
			'description' => '',
		));

	}
?>