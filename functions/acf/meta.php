<?php
/*
		Advanced Custom Fields
		- META TITLE & DESCRIPTIONS FOR PAGES & POSTS

*/
	if(function_exists("register_field_group")) {
		acf_add_local_field_group(array(
			'key' => 'group_seo_meta',
			'title' => 'Meta Content',
			'fields' => array(
				array(
					'key' => 'group_5b6158b0155ff',
					'label' => 'Meta Title',
					'name' => 'seo_title',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '100%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'group_5b6158b0365ff',
					'label' => 'Meta Description',
					'name' => 'seo_description',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '100%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
					),
				),
				array(
					array(
						'param' => 'post_type',
						'operator' => '!=',
						'value' => 'post',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'side',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

	}
?>