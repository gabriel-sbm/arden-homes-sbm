<?php
	/*
			HOME RANGES

	*/
	if( function_exists('acf_add_local_field_group') ) {
		acf_add_local_field_group(array(
			'key' => 'group_ranges',
			'title' => 'Ranges',
			'fields' => array(

				array(
					'key' => 'field_5c1172ca164ad',
					'label' => 'Ranges',
					'name' => 'ranges',
					'type' => 'repeater',
					'instructions' => 'The range logo will be an \'svg\' using the range name. (replace any spaces with hyphons)',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'field_5c1172d5164ae',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => '',
					'sub_fields' => array(
						//  
						//  
						//   ###########  ############   ##########   
						//  ####          ####          ####    ####  
						//   ##########   ########      ####    ####  
						//          ####  ####          ####    ####  
						//  ###########   ############   ##########   
						//  
						//
						array(
							'key' => 'ah-seo-tab',
							'label' => 'SEO Settings',
							'name' => '',
							'type' => 'tab',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'placement' => 'left',
							'endpoint' => 0,
						),
						array(
							'key' => 'range-seo-title',
							'label' => 'SEO Title',
							'name' => 'range_seo_title',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						/* Wysiwyg */ [
							'key' => 'range-seo-description',
							'label' => 'SEO Description',
							'name' => 'range_seo_description',
							'type' => 'wysiwyg',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => [
								'width' => '',
								'class' => '',
								'id' => '',
							],
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 1,
							'delay' => 0,
						],
						//  
						//  
						//   ###########  ############  #####   ####  ############  ###########     ########    ####          
						//  ####          ####          ######  ####  ####          ####    ####  ####    ####  ####          
						//  ####   #####  ########      #### ## ####  ########      ##########    ############  ####          
						//  ####    ####  ####          ####  ######  ####          ####    ####  ####    ####  ####          
						//   ##########   ############  ####   #####  ############  ####    ####  ####    ####  ############  
						//  
						//
						array(
							'key' => 'theme-options-ranges-general',
							'label' => 'General Settings',
							'name' => '',
							'type' => 'tab',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'placement' => 'left',
							'endpoint' => 0,
						),
						/* Checkbox */ [
							'key' => 'enable-range',
							'label' => 'Enable Range',
							'name' => 'enable_range',
							'type' => 'true_false',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => [
								'width' => '25%',
								'class' => '',
								'id' => '',
							],
							'message' => '',
							'default_value' => 1,
							'ui' => 0,
							'ui_on_text' => '',
							'ui_off_text' => '',
						],
						array(
							'key' => 'field_5c1172d5164ae',
							'label' => 'Range Name',
							'name' => 'range_name',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50%',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array(
							'key' => 'field_5c1172e9164b0',
							'label' => 'Range Colour',
							'name' => 'range_colour',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '25%',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
						),
						array(
							'key' => 'theme-options-ranges-inclusions',
							'label' => 'Inclusions PDF',
							'name' => 'range_inclusions_pdf',
							'type' => 'file',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'url',
							'library' => 'all',
							'min_size' => '',
							'max_size' => '',
							'mime_types' => '',
						),

						//  
						//  
						//  ############  ############    ########    ############  ####    ####  ###########   ############  
						//  ####          ####          ####    ####      ####      ####    ####  ####    ####  ####          
						//  ########      ########      ############      ####      ####    ####  ##########    ########      
						//  ####          ####          ####    ####      ####      ####    ####  ####    ####  ####          
						//  ####          ############  ####    ####      ####        ########    ####    ####  ############  
						//  
						// 
						array(
							'key' => 'theme-options-ranges-feature',
							'label' => 'Feature Block',
							'name' => '',
							'type' => 'tab',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'placement' => 'left',
							'endpoint' => 0,
						),
						array(
							'key' => 'ranges-feature-content',
							'label' => 'Feature Content',
							'name' => 'ranges_feature_content',
							'type' => 'wysiwyg',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 1,
							'delay' => 0,
						),
						array(
							'key' => 'ranges-feature-button-text',
							'label' => 'Feature Button Text',
							'name' => 'ranges_feature_button_text',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						//  
						//  
						//    ########    ###########     ########    ####    ####  ############  ####    ####  ############  
						//  ####    ####  ####    ####  ####    ####  ####    ####      ####      ####    ####  ####          
						//  ############  ##########    ####          ############      ####      #####  #####  ########      
						//  ####    ####  ####    ####  ####    ####  ####    ####      ####        ########    ####          
						//  ####    ####  ####    ####    ########    ####    ####  ############      ####      ############  
						//  
						//
						array(
							'key' => 'theme-options-ranges-archive',
							'label' => 'Archive Page',
							'name' => '',
							'type' => 'tab',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'placement' => 'left',
							'endpoint' => 0,
						),
						array(
							'key' => 'ranges-archive-heading',
							'label' => 'Page Heading',
							'name' => 'ranges_archive_heading',
							'type' => 'text',
							'instructions' => 'This is the main heading for the archive (H1).',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						array(
							'key' => 'ranges-archive-content',
							'label' => 'Page Content',
							'name' => 'ranges_archive_content',
							'type' => 'wysiwyg',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 1,
							'delay' => 0,
						),
						array(
							'key' => 'ranges-archive-image',
							'label' => 'Header Image',
							'name' => 'ranges_archive_image',
							'type' => 'image',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => '',
							'max_height' => '',
							'max_size' => '',
							'mime_types' => '',
						),

						//  
						//  
						//  ##########    ############   ###########    ########    ####            ########    ############  ####    ####  ############  ###########   
						//  ####    ####      ####      ####          ####    ####  ####          ####    ####      ####      #####  #####  ####          ####    ####  
						//  ####    ####      ####       ##########   ####          ####          ############      ####      ############  ########      ##########    
						//  ####    ####      ####              ####  ####    ####  ####          ####    ####      ####      #### ## ####  ####          ####    ####  
						//  ##########    ############  ###########     ########    ############  ####    ####  ############  ####    ####  ############  ####    ####  
						//  
						//
						/* Wysiwyg */ [
							'key' => 'ranges-archive-disclaimer',
							'label' => 'Disclaimer',
							'name' => 'ranges_archive_disclaimer',
							'type' => 'wysiwyg',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => [
								'width' => '',
								'class' => '',
								'id' => '',
							],
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 1,
							'delay' => 0,
						],


					),
				),

				// End
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'theme-options-ranges',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'hide_on_screen' => array(
				0 => 'the_content',
			),
			'active' => 1,
			'description' => '',
		));
	}
?>