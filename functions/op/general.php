<?php
	if( function_exists('acf_add_local_field_group') ) {
		acf_add_local_field_group(array(
			'key' => 'group_5aefd3dfc8403',
			'title' => 'General',
			'fields' => array(
				//  
				//  
				//  ####    ####  ############    ########    ##########    ############  ###########   
				//  ####    ####  ####          ####    ####  ####    ####  ####          ####    ####  
				//  ############  ########      ############  ####    ####  ########      ##########    
				//  ####    ####  ####          ####    ####  ####    ####  ####          ####    ####  
				//  ####    ####  ############  ####    ####  ##########    ############  ####    ####  
				//  
				// 
				array(
					'key' => 'field_header_tab',
					'label' => 'Header',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'header_phone',
					'label' => 'Phone number',
					'name' => 'phone',
					'type' => 'text',
					'instructions' => 'Phone number to use for \'Call ##########\'.',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array(
					'key' => 'header_by_line',
					'label' => 'By Line',
					'name' => 'by_line',
					'type' => 'text',
					'instructions' => 'Line of text to appear under the main logo.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				//  
				//  
				//  ############   ##########    ##########   ############  ############  ###########   
				//  ####          ####    ####  ####    ####      ####      ####          ####    ####  
				//  ########      ####    ####  ####    ####      ####      ########      ##########    
				//  ####          ####    ####  ####    ####      ####      ####          ####    ####  
				//  ####           ##########    ##########       ####      ############  ####    ####  
				//  
				//
				array(
					'key' => 'field_footer_tab',
					'label' => 'Footer',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'copyright_text',
					'label' => 'Copyright Text',
					'name' => 'copyright',
					'type' => 'text',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				//  
				//  
				//  ###########     ########     ###########  ############         ####          ############  #####   ####  ####    ####   ###########  
				//  ####    ####  ####    ####  ####          ####                 ####              ####      ######  ####  ####    ####  ####          
				//  ###########   ############  ####   #####  ########             ####              ####      #### ## ####  ##########     ##########   
				//  ####          ####    ####  ####    ####  ####                 ####              ####      ####  ######  ####    ####          ####  
				//  ####          ####    ####   ##########   ############         ############  ############  ####   #####  ####    ####  ###########   
				//  
				//
				array(
					'key' => 'field_5c104cc60b5ce',
					'label' => 'Footer Navigation',
					'name' => 'footer_navigation',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'field_5c104cd50b5cf',
					'min' => 0,
					'max' => 0,
					'layout' => 'row',
					'button_label' => 'Add Column',
					'sub_fields' => array(
						array(
							'key' => 'field_5c104cd50b5cf',
							'label' => 'Title',
							'name' => 'title',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array(
							'key' => 'field_5c104cdf0b5d0',
							'label' => 'Footer Links',
							'name' => 'footer_links',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'collapsed' => '',
							'min' => 0,
							'max' => 0,
							'layout' => 'table',
							'button_label' => '',
							'sub_fields' => array(
								array(
									'key' => 'field_5c104ced0b5d1',
									'label' => 'Label',
									'name' => 'label',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '25%',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
								),
								array(
									'key' => 'field_5c104cf80b5d2',
									'label' => 'Page Link?',
									'name' => 'page_link',
									'type' => 'true_false',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '11%',
										'class' => '',
										'id' => '',
									),
									'message' => '',
									'default_value' => 0,
									'ui' => 0,
									'ui_on_text' => '',
									'ui_off_text' => '',
								),
								array(
									'key' => 'field_5c104d2d0b5d3',
									'label' => 'Page',
									'name' => 'page',
									'type' => 'page_link',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_5c104cf80b5d2',
												'operator' => '==',
												'value' => '1',
											),
										),
									),
									'wrapper' => array(
										'width' => '32%',
										'class' => '',
										'id' => '',
									),
									'post_type' => array(
									),
									'taxonomy' => array(
									),
									'allow_null' => 0,
									'allow_archives' => 1,
									'multiple' => 0,
								),
								array(
									'key' => 'field_5c104d4c0b5d4',
									'label' => 'Link',
									'name' => 'link',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_5c104cf80b5d2',
												'operator' => '!=',
												'value' => '1',
											),
										),
									),
									'wrapper' => array(
										'width' => '32%',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
								),
							),
						),
					),
				),

				// End
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'theme-options-general',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}
?>