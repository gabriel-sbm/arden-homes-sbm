<?php
/**
		ADVANCED CUSTOM FIELDS - OPTIONS PAGES
		- Add Pages : https://www.advancedcustomfields.com/resources/acf_add_options_page/
		- Sub Pages : https://www.advancedcustomfields.com/resources/acf_add_options_sub_page/
		- Dashicons : https://developer.wordpress.org/resource/dashicons/
		- PHP
		var_dump(get_field('locations','options'));

	**/

	if (function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title'  => 'Theme Options',
			'menu_slug' => 'theme-options',
			'position' => 0.1,
			'parent_slug' => '',
			'icon_url' => false,
		));

		// General Content
		acf_add_options_page(array(
			'page_title'  => 'General',
			'menu_slug' => 'theme-options-general',
			'position' => false,
			'icon_url' => false,
			'parent_slug' => 'theme-options',
		));

		// Home Ranges
		acf_add_options_page(array(
			'page_title'  => 'Home Ranges',
			'menu_slug' => 'theme-options-ranges',
			'position' => false,
			'icon_url' => false,
			'parent_slug' => 'theme-options',
		));

		// Carousel
		acf_add_options_page(array(
			'page_title'  => 'Carousel',
			'menu_slug' => 'theme-options-carousel',
			'position' => false,
			'icon_url' => false,
			'parent_slug' => 'theme-options',
		));

		// Slides
		acf_add_options_page(array(
			'page_title'  => 'Slides',
			'menu_slug' => 'theme-options-slides',
			'position' => false,
			'icon_url' => false,
			'parent_slug' => 'theme-options',
		));

		// Locations
		acf_add_options_page(array(
			'page_title'  => 'Locations',
			'menu_slug' => 'theme-options-locations',
			'position' => false,
			'icon_url' => false,
			'parent_slug' => 'theme-options',
		));

		// Socials
		acf_add_options_page(array(
			'page_title'  => 'Socials',
			'menu_slug' => 'theme-options-socials',
			'position' => false,
			'icon_url' => false,
			'parent_slug' => 'theme-options',
		));

		// Email Signatures
		acf_add_options_page(array(
			'page_title'  => 'Email Signatures',
			'menu_slug' => 'theme-options-emailsig',
			'position' => false,
			'icon_url' => false,
			'parent_slug' => 'theme-options',
		));

		// acf_add_options_page(array(
		// 	'page_title'  => 'SEO',
		// 	'menu_slug' => 'theme-options-seo',
		// 	'position' => false,
		// 	'icon_url' => false,
		// 	'parent_slug' => 'theme-options',
		// ));

	}

?>