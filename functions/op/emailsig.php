<?php
	/*
			EMAIL SIGNATURES

	*/
	if( function_exists('acf_add_local_field_group') ) {
		acf_add_local_field_group(array(
			'key' => 'email_signatures_group',
			'title' => 'Content',
			'fields' => array(

				// Images
				array(
					'key' => 'theme-options-images-tab',
					'label' => 'Images',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'email-signature-repeater',
					'label' => 'Images',
					'name' => 'signature_images',
					'type' => 'repeater',
					'instructions' => 'The maximum number of banners is 4.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 4,
					'layout' => 'table',
					'button_label' => '',
					'sub_fields' => array(
						array(
							'key' => 'signature_image',
							'label' => 'Image',
							'name' => 'image',
							'type' => 'image',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => '',
							'max_height' => '',
							'max_size' => '',
							'mime_types' => '',
						),
					),
				),

				// Socials
				array(
					'key' => 'theme-options-socials-tab',
					'label' => 'Social Links',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'email-social-repeater',
					'label' => 'Socials',
					'name' => 'signature_socials',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'social-link-title',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => '',
					'sub_fields' => array(

						// Checkbox
						array(
							'key' => 'field_social_check',
							'label' => 'Use Social Icon',
							'name' => 'use_social_icon',
							'type' => 'true_false',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'message' => '',
							'default_value' => 1,
							'ui' => 0,
							'ui_on_text' => '',
							'ui_off_text' => '',
						),
						// Link Title
						array(
							'key' => 'social-link-title',
							'label' => 'Link Title',
							'name' => 'social_link_title',
							'type' => 'text',
							'instructions' => 'This is the title for the link.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50%',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						// Link URL
						array(
							'key' => 'social-link-url',
							'label' => 'Link URL',
							'name' => 'social_link_url',
							'type' => 'text',
							'instructions' => 'URL link for social.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50%',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						// Link Icon Alt tag
						array(
							'key' => 'social-alt-tag',
							'label' => 'Icon Alt Tag',
							'name' => 'social_link_alt_tag',
							'type' => 'text',
							'instructions' => 'This is the alt tag for the icon.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50%',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						// Image
						//
						array(
							'key' => 'social-icon-image',
							'label' => 'Feature Image',
							'name' => 'social_icon_image',
							'type' => 'image',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50%',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => '',
							'max_height' => '',
							'max_size' => '',
							'mime_types' => '',
						),
					),
				),

				// End
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'theme-options-emailsig',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => array(
				0 => 'the_content',
			),
			'active' => 1,
			'description' => '',
		));
	}
?>