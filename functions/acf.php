<?php
	/*
			Advanced Custom Fields
			- Load all files located in the 'acf' folder
	*/
	if ($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/projects/framework/wp-content/themes/mega/functions/acf')) {
		$blacklist = array('.', '..', '.DS_Store', '_temp', 'index.php', 'index.html');
		while (false !== ($file = readdir($handle))) {
			if (!in_array($file, $blacklist)) {
				include_once "acf/{$file}";
			}
		}
		closedir($handle);
	}
?>