<?php




// Allow SVG in Media Library
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

  /*
			Add in the code integrations for theme options.
      Theme Options -> SEO:
        * Code Integrations:
            - Google Analytics - (Universal/Tag)
            - Delacon Call Tracking
            - Google Remarketing
        * Gravity Forms - Goal Tracking
            - Select the form in the drop down and add the appropriate Google goal tracking code.
            - The code is executed on successful form submissions of the selected form.
	*/

  function sbm_code_integrations_header() {

    $code_integrations = get_field('code_integrations', 'option');
    if($code_integrations){
		foreach ($code_integrations as &$code_integration) {
			if($code_integration['active'] == 1 && $code_integration['add_to'] == 'header'){
				if($code_integration['code'] != ''){
					echo "\n<!-- " . $code_integration['type'] . " START -->\n<script>\n";
					echo $code_integration['code'];
					echo "\n</script>\n<!-- " . $code_integration['type'] . " END -->\n";
				}
			}
		}
	}

  }
  add_action('wp_head', 'sbm_code_integrations_header', 100);

  function sbm_code_integrations_footer() {
    $code_integrations = get_field('code_integrations', 'option');
	if($code_integrations){
		foreach ($code_integrations as &$code_integration) {
			if($code_integration['active'] == 1 && $code_integration['add_to'] == 'footer'){
				if($code_integration['code'] != ''){
					echo "\n<!-- " . $code_integration['type'] . " START -->\n<script>\n";
					echo $code_integration['code'];
					echo "\n</script>\n<!-- " . $code_integration['type'] . " END -->\n";
				}
			}
		}
	}
    
  }
  add_action('wp_footer', 'sbm_code_integrations_footer', 100);


  function sbm_code_integrations_after_form_submission( $entry, $form ) {
    $form_id = rgar( $form, 'id' );
    $gravity_forms_goal_tracking = get_field('gravity_forms_goal_tracking', 'option');
    if($gravity_forms_goal_tracking){
		foreach ($gravity_forms_goal_tracking as &$gravity_form_goal_tracking) {
			if( $gravity_form_goal_tracking['active'] == 1 && $form_id == $gravity_form_goal_tracking['form']){
				if( $gravity_form_goal_tracking['code_snippet'] != '' ){
					echo "\n<!-- GF:" . $gravity_form_goal_tracking['form'] . " START -->\n<script>\n";
					echo $gravity_form_goal_tracking['code_snippet'];
					echo "\n</script>\n<!-- GF:" . $gravity_form_goal_tracking['form'] . " END -->\n";
				}
			}
		}
	}
  }


	/*
			GOOGLE MAPS

	*/
	function my_acf_google_map_api( $api ){
	  $api['key'] = 'AIzaSyD5JeKCNcNwcKOOOkgnF5NOhR9fRfBKQeo';
	  return $api;
	}

	add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


	/* Highlight 'BOSS' if it exists and no Position is defined.
			...otherwise if position is supplied:
	    Positive : Starting from the left with the first word being at position 0
	    Negative : Starting from the right with the last word being at position -1

	*/
	function embolden($str,$pos=null){
		// if (strpos(strtolower($str),'boss') > -1 && is_null($pos)) {
		// 	$str = str_replace('boss','<strong>boss</strong>',$str);
		// 	$str = str_replace('Boss','<strong>Boss</strong>',$str);
		// } else {
		// 	if (is_null($pos)) {
		// 		$pos = 0;
		// 	}
		// 	$arr = explode(' ',trim($str));
		// 	if ($pos >= 0) {
		// 		for ($i=0; $i<=$pos; $i++) {
		// 			$arr[$i] = "<strong>{$arr[$i]}</strong>";
		// 		}
		// 	} else {
		// 		for ($i=-1; $i>=$pos; $i--) {
		// 			$arr[count($arr)+$i] = "<strong>{$arr[count($arr)+$i]}</strong>";
		// 		}
		// 	}
		// 	$str = implode(' ',$arr);
		// }
		return $str;
	}

	// Return image object to be used as required
	function validateImage($img, $sizes=['w'=>2000,'h'=>500]) {
		if (is_null($sizes) || empty($sizes)) {
			$sizes = ['w'=>2000,'h'=>500];
		}
		if (is_null($img) || empty($img) || is_null($img['url']) || empty($img['url'])) {
			$img['url'] = "https://ph4se.com.au/app/image/?text=Placeholder%20{$sizes['w']}%20x%20{$sizes['h']}px&w={$sizes['w']}&h={$sizes['h']}";
			$img['alt'] = "Placeholder {$sizes['w']} x {$sizes['h']}px";
		}
		return $img;
	}








?>
