<?php
	get_header();

	// CONTENT ------------------------------------------------------------------
?>
	<div class="content search-layout">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1><strong>Search</strong> Results</h1>
					<?php get_search_form(); ?>
				</div>
				<div class="col-sm-8">
					<div class="main">
						<h2 class="search-title" style="margin-top:20px;">
						<?php
							$query = get_search_query();
							if (!is_null($query) && !empty($query)) {
						?>
							<?php echo $wp_query->found_posts; ?> <?php _e( 'Search Results Found For', 'locale' ); ?>: "<?php the_search_query(); ?>"
						<?php
							} else {
								echo "Default results:";
							}
						?>
						</h2>
				 		<?php if ( have_posts() ) : ?>
					 	<ol class="list-unstyled list-archives">
							<!-- Loop -->
							<?php while ( have_posts() ) : the_post(); ?>
							<li>
								<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
								<div class="meta">Posted: <?php the_time('F j Y') ?></div>
								<?php the_excerpt(); ?>
							</li>
							<?php endwhile; ?>
					 	</ol>

		        <?php else : ?>
	            <p>No posts available</p>
		        <?php endif; ?>
					</div>
				</div>

			</div>
		</div>
	</div>

<?php
	get_footer();
?>