<?php
	/* Template Name: Page - Displays */

	// Header
	get_header();

	$features =	FeatureDisplay::FromRepeaterObject(get_field('display_homes'), 'display');

	$office = [
		'address' => [
			'lat' => -37.808163434,
			'lng' => 144.957829502,
		],
	];

	// print_r($features);

?>
		<div class="google-map">
			<div class="container">
		  	<div class="acf-map">
		  		<?php
		  			foreach ($features as $feature) {
			  			echo "
							<div class=\"marker\" data-lat=\"{$feature->coordinates['lat']}\" data-lng=\"{$feature->coordinates['lng']}\" name=\"{$feature->name}\"></div>
			  			";
			  		}
					?>
  			</div>
  		</div>
		</div>
<?php


	// Content Panel
	$panel_class = "";
	$panel_heading = get_field('displays_heading');
	$panel_content   = get_field('displays_intro');
	include("module/content-panel.php");

	// Display Features
	FeatureDisplay::OutputFeaturesWithClass('display_homes',$features);


	// Footer
	get_footer();

?>