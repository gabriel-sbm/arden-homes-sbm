<?php
	get_header(); 

?>
		<div class="content default-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1>404: Page not Found</h1>
						<div class="intro">
							<p>Sorry, but the page you are looking for has not been found. Try checking the URL for errors, then hit the refresh button on your browser. Alternatively select one of the links above.</p>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
	
<?php
	get_footer();

?>