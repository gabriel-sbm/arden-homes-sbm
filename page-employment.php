<?php
	/* Template Name: Page - Employment */

	// Header
	get_header();

	if (have_posts()) {
		while (have_posts()) {
			the_post();

			$image = get_field('default_hero_image');
			if (!empty($image)) {
				$slide = Slide::WithImage($image);
				$slide->title = get_field('default_hero_caption');

				// CAROUSEL
				$carousel = new Carousel();
				$carousel->slides[] = $slide;
				include('module/carousel/carousel.php');
			}

	?>
		<div class="content default-layout employment-layout">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<h1><?php the_title(); ?></h1>
						<?php
							$intro = get_field('default_introduction');
							if (!is_null($intro) && !empty($intro)) {
								echo "
									<div class=\"intro\">
										{$intro}
									</div>
								";
							}
							$content = get_field('default_content');
							if (!is_null($content) && !empty($content)) {
								echo $content;
							}
						?>
					</div>
					<div class="col-12 col-lg-6">
						<?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php
		}
	}
	?>
	</div>
<?php
	// Footer
	get_footer();
?>