<?php
	/* Template Name: Page - New Homes */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('nh-carousel-panel', 'slide');
	include('module/carousel/carousel.php');

	// Content Panel
	$panel_class = "";
	$panel_heading = get_field('page_heading');
	$panel_intro = get_field('introduction');
	$panel_content = get_field('content');
	include("module/content-panel.php");

	// RANGES
	$ranges = Range::GetRanges();
	if (count($ranges) > 0) {
		$cols = 0;
		for ($i=0; $i<count($ranges); $i++) { 
			if ($ranges[$i]->enabled) $cols++;
		}
		$cols = 12 / $cols;
		echo "
			<section class=\"ranges-feature\">
				<div class=\"container\">
					<ul class=\"row\">
		";
		foreach ($ranges as $range) {
			if ($range->enabled) {
				echo "
						<li class=\"col-12 col-lg-{$cols}\">
							<a href=\"".get_bloginfo('url')."/category/".strtolower($range->name)."\" style=\"background-color:{$range->colour}\">
								<img class=\"svg\" src=\"".get_bloginfo('template_url')."/assets/images/logos/{$range->logo}.svg\" height=\"90\" alt=\"{$range->name} by Arden\">
								<div class=\"placeholder-image\" style=\"background-image:url({$range->archive_image->url});\"></div>
								<div class=\"hero-image\" style=\"background-image:url({$range->archive_image->url});\">
									<div class=\"cover\" style=\"background-color:{$range->colour}\"></div>
									<div class=\"feature-content\">
										{$range->feature_content}
										<p class=\"button\">".strtoupper($range->feature_button_text)."</p>
									</div>
								</div>
							</a>
						</li>
				";
			}
		}
		echo "
					</ul>
				</div>
			</section>
		";


	}

	// Footer
	get_footer();

?>