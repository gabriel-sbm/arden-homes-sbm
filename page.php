<?php
	/* Template Name: Page - Default */

	// Header
	get_header();

	if (have_posts()) {
		while (have_posts()) {
			the_post();

			$image = get_field('default_hero_image');
			if (!empty($image)) {
				$slide = Slide::FromPostWithPre("default_hero");
				// CAROUSEL
				$carousel = new Carousel();
				$carousel->slides[] = $slide;
				$carousel->captions = true;
				include('module/carousel/carousel.php');
			}
			// Get Page Content
			$hide_h1 = get_field('default_hide_h1');
			$intro   = get_field('default_introduction');
			$content = get_field('default_content');
			// If no content, hide
			if (!$hide_h1 || (!is_null($intro) && !empty($intro)) || (!is_null($content) && !empty($content))) {
				if (!is_null($content) && !empty($content) && strpos($content,'send-ontraport')) {
					// Content Ontraport
					$entities = ["##send-ontraport##","<p>","</p>"];
					$replacements = ["","",""];
					foreach ($_GET as $key => $value) {
						$entities[] = "##{$key}##";
						$replacements[] = str_replace("'", "%27", $value);
					}
					$content = str_replace($entities, $replacements, $content);
					echo "<img style=\"position:absolute;left:0;visibility:hidden;\" src=\"https://ardenhomes.ontraport.com/p?order_id=Newsletter%20;subscription&amp;{$content}\">";
				} else {
					// Standard page
	?>
		<div class="content default-layout small-fonts">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php 
							// Title
							if (!$hide_h1) {
								echo "<h1>".get_the_title()."</h1>";
							}
							// Intro
							if (!is_null($intro) && !empty($intro)) {
								echo "
									<div class=\"intro\">
										{$intro}
									</div>
								";
							}
							// Content
							if (!is_null($content) && !empty($content)) {
								echo $content;
							}
						?>
					</div>
				</div>
			</div>
		</div>
	<?php
				}
			}
		}
	}
	?>
	</div>
<?php
	// Footer
	get_footer();
?>