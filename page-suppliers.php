<?php
	/* Template Name: Page - Suppliers */

	// Header
	get_header();

	if (have_posts()) {
		while (have_posts()) {
			the_post();

			$image = get_field('supplier_hero_image');
			if (!empty($image)) {
				$slide = Slide::FromPostWithPre("supplier_hero");
				// CAROUSEL
				$carousel = new Carousel();
				$carousel->slides[] = $slide;
				$carousel->captions = true;
				include('module/carousel/carousel.php');
			}
			// Get Page Content
			$hide_h1 = get_field('supplier_hide_h1');
			$intro   = get_field('supplier_introduction');
			$content = get_field('supplier_content');
			// If no content, hide
			if (!$hide_h1 || (!is_null($intro) && !empty($intro)) || (!is_null($content) && !empty($content))) {
				if (!is_null($content) && !empty($content) && strpos($content,'send-ontraport')) {
					// Content Ontraport
					$entities = ["##send-ontraport##","<p>","</p>"];
					$replacements = ["","",""];
					foreach ($_GET as $key => $value) {
						$entities[] = "##{$key}##";
						$replacements[] = str_replace("'", "%27", $value);
					}
					$content = str_replace($entities, $replacements, $content);
					echo "<img style=\"position:absolute;left:0;visibility:hidden;\" src=\"https://ardenhomes.ontraport.com/p?order_id=Newsletter%20;subscription&amp;{$content}\">";
				} else {
					// Standard page
	?>
		<div class="content default-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
					    <div>
						<?php 
							// Title
							if (!$hide_h1) {
								echo "<h1>".get_the_title()."</h1>";
							}
							// Intro
							if (!is_null($intro) && !empty($intro)) {
								echo "
									<div class=\"intro\">
										{$intro}
									</div>
								";
							}
							// Content
							if (!is_null($content) && !empty($content)) {
								echo $content;
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
				}
			}
		}
	}

	//
	// Suppliers
	//
	$suppliers = get_field('supplier-repeater');
	if (is_array($suppliers) && count($suppliers) > 0) {
		echo "
		<div class=\"content suppliers-layout\">
			<div class=\"container\">
		";
		$heading = get_field('supplier_heading');
		$screen  = get_field('supplier_screen_reader');
		$type    = get_field('supplier_heading_type');
		// Wrap Heading
		if (empty($type)) $type = "h2";
		$heading = "<{$type}".(($screen)?" class=\"sr-only\"":"").">$heading</{$type}>";

		if (!empty($heading)) {
			echo "
				{$heading}
			";
		}

		echo "
				<ul class=\"row suppliers-list\">
		";
		foreach ($suppliers as $supplier) {
			$name  = $supplier['supplier_name'];
			$link  = $supplier['supplier_link'];
			$image = $supplier['supplier_image'];

			if (!preg_match('/http+s?:\/\//', $link)) {
				$link = "http://{$link}";
			}

			// DESKTOP
			if (!empty($link)) {
				echo "
					<li class=\"col-6 col-lg-2 supplier-linked\"><a href=\"{$link}\" target=\"_blank\"><img src=\"{$image['url']}\" alt=\"{$name}\"></a></li>
				";
			} else {
				echo "
					<li class=\"col-6 col-lg-2\"><img src=\"{$image['url']}\" alt=\"{$name}\"></li>
				";
			}
		}
		echo "
				</ul>
			</div>
		</div>
		";
	}
	?>
	</div>
<?php
	// Footer
	get_footer();
?>