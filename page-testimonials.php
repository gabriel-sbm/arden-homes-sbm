<?php

/* Template Name: Page - Testimonials */

// Header
get_header();


if (have_posts()) {
	while (have_posts()) {
		the_post();
?>
		<div class="content blog-post">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php

						// check if the repeater field has rows of data
						if (have_rows('videos_repeater')) :

							// loop through the rows of data
							while (have_rows('videos_repeater')) : the_row(); ?>
								<div class="row inner-row">
									<div class="col-6">
										<div class="card-deck">
											<div class="card">
												<a data-fancybox href="https://www.youtube.com/embed/<?php the_sub_field('video_id') ?>">
													<div class="thumbnail-icon"></div>												
													<img src="<?php the_sub_field('video_thumbnail'); ?>">
												</a>
											</div>
										</div>																		
									</div>
									<div class="col-6">
										<div class="card-deck">
											<div class="card-body">
												<h2><?php the_sub_field('video_title'); ?></h2>
												<p class="card-text"><?php the_sub_field('video_desc'); ?></p>
											</div>
										</div>									
									</div>
								</div>
						<?php endwhile;

						else :
						// no rows found

						endif;

						?>
					</div>
				</div>
			</div>
		</div>
<?php
	}
}
?>

</div>

<?php get_footer(); ?>
