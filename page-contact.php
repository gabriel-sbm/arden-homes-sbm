<?php
	/* Template Name: Page - Contact */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('ch-carousel-panel', 'slide');
	$carousel->navigation = 1;
	$carousel->indicator = 'none';
	include('module/carousel/carousel.php');

	// Content Panel
	$panel_class = "";
	$panel_heading = get_field('contact_heading');
	$panel_intro   = get_field('contact_intro');
	$panel_content = null;
	include("module/content-panel.php");

	// Content
	$panel_content = get_field('contact_content');
?>
			<section class="contact-module">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-7 contact-block">
							<?=$panel_content?>
						</div>
						<div class="col-12 col-lg-5">
							<?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="false"]'); ?>
						</div>
					</div>
				</div>
			</section>

<?php

	// Footer
	get_footer();

?>