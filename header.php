<?php
	include_once('module/class/bobject.php');
	include_once('module/class/carousel.php');
	include_once('module/class/slide.php');
	include_once('module/class/content.php');
	include_once('module/class/feature-group.php');
	include_once('module/class/feature.php');
	include_once('module/class/sbm-image.php');
	include_once('module/class/cta.php');
	include_once('module/class/range.php');
	include_once('module/class/home.php');
	include_once('module/class/feature-item.php');
	include_once('module/class/feature-display.php');
	include_once('module/class/contact.php');

	$buster = time();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<?php
		//  META VALUES FOR POST/PAGE - META FIELD GROUP 
		global $post;
		$metaTitle = "";
		$metaDesc = "";
		try {
			if(!function_exists('get_field')) {
				throw new Exception('ACF not Active.');
			}
			if( is_home() ){
				$blogID = get_site_option( 'page_for_posts' );
				if( !get_field( 'seo_title', $blogID ) ){
					$metaTitle = get_the_title( $blogID );
				} else{
					$metaTitle = get_field( 'seo_title', $blogID );
					$metaDesc =  get_field( 'seo_description', $post->ID );
				}
			} else {
				if( is_archive() ){
					$term = get_queried_object();
					// Category
					if(isset($term->term_id)) {
						$range = Range::GetRange($term->name);
						$metaTitle = isset($range->seo_title) ? $range->seo_title : "";
						$metaDesc  = isset($range->seo_desc) ? $range->seo_desc : "";
					}
					// Custom Post Type
					if (empty($metaTitle)) {
						$name = $term->name;
						$metaTitle = get_field("{$name}_seo_title", "options");
						$metaDesc  = get_field("{$name}_seo_description", "options");
					}

				} else{
					if( !get_field( 'seo_title', $post->ID ) ){
						$metaTitle = get_the_title( $post->ID );
					} else{
						$metaTitle = get_field( 'seo_title', $post->ID );
						$metaDesc =  get_field( 'seo_description', $post->ID );
					}
				}


			}
		} catch(Exception $e) {
			echo "<div style=\"color:#900; font-weight:bold; font-size:20px;\">This requires Advanced Custom Fields (ACF) plugin to be Active!</div>";
		}
		if (!empty($metaTitle)) {
			$metaTitle .= " | ";
		}
		$metaTitle .= get_bloginfo( 'name' );

	?>
	<title><?php echo $metaTitle; ?></title>

		<!-- Meta -->
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="description" content="<?=$metaDesc?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="profile" href="http://gmpg.org/xfn/11" />

		<!-- Icons -->
		<link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
		<link rel="apple-touch-startup-image" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
		<link rel="apple-touch-icon-precomposed" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-icon-57.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-icon-114.png" />

		<!-- Fonts -->
		<link rel="stylesheet" href="https://use.typekit.net/ohl2qqg.css">
		<!-- Georgia for windows -->
		<link href="//db.onlinewebfonts.com/c/7dca09e227fdfe16908cebb4244589e4?family=Georgia" rel="stylesheet" type="text/css"/>

		<!-- CSS -->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?version=<?=$buster?>" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/swipebox.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/jasny-bootstrap.min.css">
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/assets/js/full_width.js?version=<?=$buster?>"></script>
		<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.swipebox.min.js"></script>
		
		<!-- Fancybox - css -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">

		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.placeholder.js"></script>
		<![endif]-->

		<script>
			// IE @media fix
			if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
				var msViewportStyle = document.createElement("style")
				msViewportStyle.appendChild(
					document.createTextNode(
						"@-ms-viewport{width:auto!important}"
					)
				)
				document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
			}

			// Compress Header
			<?php
				$scroll_top = 0;
				if (is_front_page()) {
					$scroll_top = 50;
				}
			?>
			var scroll_top = <?php echo $scroll_top; ?>;
			var bannerHeight = 1000;
			var menuHeight = 100;

			jQuery(document).scroll(function($) {
				// Scroll to bottom of banner
				// menuHeight = $('.header').height() + 1; // +1 forces the heading transition
				// if ($(document).scrollTop() > bannerHeight - menuHeight) { }
				if (jQuery(document).scrollTop() >= scroll_top) {
					jQuery('.desktop-nav').addClass('alt-header');
				} else {
					jQuery('.desktop-nav').removeClass('alt-header');
				}
			});

		</script>

		<!-- Wordpress Header -->
		<?php wp_head(); ?>

</head>

<?php
	$body_class_array = 
	$byline = get_field('header_by_line', 'options');
	if (is_null($byline) || empty($byline)) {
		if (!is_array($class)) $class = [];
		$class[] = "byline-none";
	}
?>
<body <?php body_class($class); ?>>
	<!--[if lt IE 7]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->
	<a href="#content" class="sr-only">Skip to main content</a>
	<div class="cover" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav"></div>
	<?php
		/** OPTIONS
		 * @header-hamburger
		 * @header-full-width
		 * @header-stacked
		 * @header-stacked-full
		**/
		include("module/header/header-stacked.php");
	?>

	<div class="wrap">

		
