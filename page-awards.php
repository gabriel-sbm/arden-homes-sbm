<?php
	/* Template Name: Page - Awards */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('awh-carousel-panel', 'slide');
	$carousel->navigation = 1;
	$carousel->indicator = 'none';
	include('module/carousel/carousel.php');

	$showing_awards = false;

	$award_intro = get_field('awards_introduction');
	$award_content = get_field('awards_content');

	echo "
		<div class=\"awards-content\">
			<div class=\"container\">
				<h1>AWARDS</h1>
	";
	if (!empty($award_intro)) {
		echo $award_intro;
	}
	if (!empty($award_content)) {
		echo $award_content;
	}
	echo "
				</div>
			</div>
		</div>
	";
    
	
	$award_list = [];
	
	
	//gets awards from Page 'Awards' , post id : 198
	$static_awards = get_field('awards_repeater', '198');

	
	if (!empty($static_awards)) {
		foreach ($static_awards as $award) {
			$award_image_url = $award['award_image'];
			$award_year = $award['award_year'];
			$award_title = $award['award_title'];
			$award_home_title = $award['award_home_title'];
			$award_home_excerpt = $award['award_home_excerpt'];
			$award_home_url = $award['award_home_link'];
			
			$home_id = get_the_ID();
			$image_url = 0;
			
			if (!is_array($award_list[$award_year])) {
				$award_list[$award_year] = [];
			}
			$award_list[$award_year][] = "
						<li class=\"col-12\">
							<div class=\"row\">
								<div class=\"award_logo\">
									<img src=\"{$award_image_url}\" alt=\"\">
								</div>
								<div class=\"award_copy\">
									<h2><strong>{$award_year}:</strong> {$award_title}</h2>
									<strong>{$award_home_title}</strong><br>
									{$award_home_excerpt}<br>
									".(($award_home_url!= null)? "<a href=\"{$award_home_url}\">View Home</a>": "")."
									</div>
							</div>
						</li>
			";
		}
	}
	
	$homes = new WP_query(
		[
			'post_type' => 'home',
			'posts_per_page' => -1,
			'orderby' => 'post_title',
			'order' => 'ASC'
		]
	);
	
	while ( $homes->have_posts() ) {
  	$homes->the_post();


		$awards = get_field('awards_repeater');
		if (!$showing_awards && !empty($awards)) {
			$showing_awards = true;
			echo "
				<section class=\"awards_list\">
					<div class=\"container\">
						<ul class=\"row\">
			";
		}
		if (!empty($awards)) {
			foreach ($awards as $award) {
				$award_data = $award['awards'][0];
				$award_image_id = get_post_thumbnail_id($award_data->ID);
				$award_image_url = wp_get_attachment_image_src($award_image_id, 'full');
				$home_id = get_the_ID();
				$image_url = 0;
				if (!is_array($award_list[$award_data->award_year])) {
					$award_list[$award_data->award_year] = [];
				}
				$award_list[$award_data->award_year][] = "
							<li class=\"col-12\">
								<div class=\"row\">
									<div class=\"award_logo\">
										<img src=\"{$award_image_url[$image_url]}\" alt=\"\">
									</div>
									<div class=\"award_copy\">
										<h2><strong>{$award_data->award_year}:</strong> {$award['award_title']}</h2>
										<strong>{$award_data->post_title}</strong><br>
										{$award['award_description']}<br>
										<a href=\"" . get_the_permalink() . "\">View Home</a>
										</div>
								</div>
							</li>
				";
			}
		}
	}
	

	// Output Awards
	krsort($award_list);
	foreach ($award_list as $awards) {
		foreach ($awards as $award) {
			echo $award;
		}
	}

	echo "
						</ul>

						<p class=\"medium-font\">
							With so many awards an commendations in different categories, you can be confident of selecting an award-winning design.
						</p>

					</div>
				</section>
	";
	wp_reset_query();

	// Features
	$features =	FeatureDisplay::FromRepeaterObject(get_field('display_homes'), 'display');
	FeatureDisplay::OutputFeaturesWithClass('display_homes',$features);

	// Footer
	get_footer();

?>