<?php
	/* Template Name: Page - Category */

	// Redirect on page
	//
	$escape = false;
	$url = $_SERVER['REQUEST_URI'];
	if (strtolower($url) == "/category/escape/") {
		$escape = true;
		if (!isset($_GET['stage'])) {
			header("Location: https://www.ardenhomes.com.au/new-homes/");
			die();
		}
	}

	// Header
	get_header();

	// Hero
	$term = get_queried_object();
	$range = Range::GetRange($term->name);

	if (!is_null($range)) {
?>
		<section class="hero-panel">
			<div class="container">
				<div class="row">
					<div class="col-12" style="background-color:<?=$range->colour?>;">
						<div class="hero-image" style="background-image:url(<?=$range->archive_image->url?>);"></div>
						<img class="svg" src="<?php bloginfo('template_url'); ?>/assets/images/logos/<?=$range->logo?>.svg" height="90" alt="<?=$range->name?> by Arden">
					</div>
				</div>
			</div>
		</section>
<?php
		// Content Panel
		$panel_class = "";
		$panel_heading = $range->archive_heading;
		$panel_content = $range->archive_content;
		include("module/content-panel.php");
	}
?>

		<section class="ranges-feature home-range">
			<div class="container">
<?php
	// Homes In Range
	$loop = new WP_Query(
		array(
			'post_type' => 'home',
			'category_name' => $range->name,
			'orderby' => 'title',
			'order' => 'ASC',
			'posts_per_page' => -1,
		)
	);
	if ($loop->have_posts()) {
		echo "
				<ul class=\"row\">
		";
		while ( $loop->have_posts() ) {
			$loop->the_post();
			$home = Home::FeatureFromPage();
			$home->OutputFeature($range);
		}
		echo "
				</ul>
		";

		if (!empty($range->archive_disclaimer)) {
			echo "
				<div class=\"disclaimer\">
					{$range->archive_disclaimer}
				</div>
			";
		}

	} else {
		echo "
				<div class=\"row\">
					<div class=\"col-12\">
						<h2>There are no Homes for this Range at this time.</h2>	
					</div>
				</div>
		";
	}
	wp_reset_query();
?>
			</div>
		</section>





<?php
	// Footer
	get_footer();

?>