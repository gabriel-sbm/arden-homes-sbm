<?php
	// Header
	get_header();

	if (have_posts()) {
?>
		<div class="content blog-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="sr-only">Resources</h1>
<?php
		if (isset($_GET['stage'])) {
?>			
			<ul class="filter_list">
			  <?php
			    foreach (get_categories() as $category) {
			      // if ($category->slug != 'uncategorised') {
print_r($category);
			        echo "<li class=\"filter_{$category->slug}\"><a href=\"#{$category->slug}\">{$category->name}</a></li>";
			      // }
			    }
			    wp_reset_query(); // May be necessary to reset
			  ?>
			</ul>

<?php

		}

		while (have_posts()) {
			the_post();
			$img = get_the_post_thumbnail_url();

			echo "
				<div class=\"row blog-item\">
					<div class=\"col-12 col-lg-4 blog-image\">
						<img src=\"{$img}?image={$index}\" alt=\"\">
					</div>
					<div class=\"col-12 col-lg-8 blog-content\">
						<h2>".get_the_title()."</h2>
						".get_the_excerpt()."
						<p><a class=\"btn btn-secondary btn-continue\" href=\"".get_the_permalink()."\">Continue reading &gt;</a></p>
					</div>
				</div>
			";
		}

		the_posts_pagination([
			'prev_text' => __( 'Newer Posts', 'textdomain' ),
		  'next_text' => __( 'Older Posts', 'textdomain' ),
		]);
?>
					</div>
				</div>
			</div>
		</div>
<?php
	}
?>

	</div>
<?php
	// Footer
	get_footer();
?>