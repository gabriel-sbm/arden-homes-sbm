<?php
	// CacheBuster
	$buster = time();

	if (!is_null(get_field('footer_separator')) && !isset(get_queried_object()->term_taxonomy_id)) {
		$footer_separator = get_field('footer_separator');
		if ($footer_separator == 1) {
			echo "
				<section class=\"content-panel footer-separator\">
					<div class=\"container\">
						<div class=\"row\">
							<div class=\"col-12\">
								<hr>
							</div>
						</div>
					</div>
				</section>
			";
		}
	}


	// CTA
	$home_id = get_option('page_on_front');
	$module = CallToAction::FromPageIdWithPre('action-panel', $home_id);
	$module->button_modal = "subscribe";
	include('module/layout/cta.php');
?>

	<section class="modal" id="subscribe">
	  <div class="modal-dialog">
  	  <div class="modal-content">
	      <div class="modal-body">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h1>Arden Homes Newsletter</h1>
	        <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true" tabindex="99"]'); ?>
	      </div>
	    </div>
	  </div>
	</section>


<?php
	// FOOTER -------------------------------------------------------------------------
	$offices = get_field('locations','options');
	$office = $offices[0];

	// Instagram Feed
?>
	<div class="insta-feed container">
		<div class="row">
			<div class="col-12">
				<?php echo do_shortcode('[instagram-feed colsmobile=2]'); ?>
			</div>
		</div>
	</div>

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="sr-only">Footer</h2>
				</div>
				<div class="col-12 footer-navigation">
					<div class="container">
						<div class="row">

				<?php
					include('module/social.php');

					$navigation = get_field('footer_navigation','options');
					if (count($navigation) > 0) {
						$cols = 12 / count($navigation);
						foreach ($navigation as $column) {
							echo "
							<div class=\"col-12 col-lg-{$cols} footer-links\">
								<h3>{$column['title']}</h3>
								<ul>
							";
							foreach ($column['footer_links'] as $item) {
								$link = isset($item['page'])?$item['page']:"";
								$blank = "";
								if ($item['page_link']!=1) {
									$link  = $item['link'];
									$blank = " target=\"_blank\"";
								}
								echo "<li><a href=\"{$link}\"{$blank}>{$item['label']}</a></li>";
							}
							echo "
								</ul>
							</div>
							";
						}
					}

				?>
						</div>
					</div>
				</div>
				<div class="col-12">
    	    <a class="footer-brand" href="<?=get_site_url()?>">
  	        <img class="svg" src="<?php bloginfo('template_url'); ?>/assets/images/arden-logo-black.svg" alt="">
	        </a>
				</div>
				<div class="col-12 copyright">
					<?=get_field('copyright','options');?>
				</div>
			</div>
		</div>
	</footer>

	<!-- Javascript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5JeKCNcNwcKOOOkgnF5NOhR9fRfBKQeo"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/script.min.js?version=<?=$buster?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
	<script>
		//  Set caption from card text
		jQuery('.card-deck a').fancybox({
		caption : function( instance, item ) {
			return $(this).parent().find('.card-text').html();
		}
		});
	</script>
	<!-- Wordpress Footer -->
	<?php wp_footer(); ?>
</body>
</html>
