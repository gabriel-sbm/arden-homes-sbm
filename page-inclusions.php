<?php
	/* Template Name: Page - Inclusions */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('in-carousel-panel', 'slide');
	include('module/carousel/carousel.php');

	// Content Panel
	$panel_class = "";
	$panel_heading = get_field('inclusions_page_heading');
	$panel_intro = get_field('inclusions_introduction');
	$panel_content = get_field('inclusions_content');
	include("module/content-panel.php");

	// RANGES
	$ranges = Range::GetRanges();
	if (count($ranges) > 0) {
		$cols = 0;
		for ($i=0; $i<count($ranges); $i++) { 
			if ($ranges[$i]->enabled) $cols++;
		}
		$cols = 12 / $cols;
		echo "
			<section class=\"ranges-inclusions\">
				<div class=\"container\">
					<ul class=\"row\">
		";
		foreach ($ranges as $range) {
			if ($range->enabled) {
				echo "
						<li class=\"col-12 col-lg-{$cols}\">
							<a href=\"{$range->inclusions}\" target=\"_blank\" style=\"background-color:{$range->colour}\">
								<img class=\"svg\" src=\"".get_bloginfo('template_url')."/assets/images/logos/{$range->logo}.svg\" height=\"90\" alt=\"{$range->name} by Arden\">
								<p>
									Inclusions Brochure<br>
									<span>DOWNLOAD</span>
								</p>
							</a>
						</li>
				";
			}
		}
		echo "
					</ul>
				</div>
			</section>
		";


	}

	// Footer
	get_footer();

?>