<?php
	/* Template Name: Page - Home */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromOptions('carousel_home');
	include('module/carousel/carousel.php');

	// TESTIMONIALS
	include('module/testimonials.php');

	// PANEL LINKS - Content Panel
	$module = FeatureGroup::FromPageWithPre('panel-links');
	include('module/layout/feature-group.php');

	// FOOTER
	get_footer();

?>
