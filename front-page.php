<?php
	/* Template Name: Page - Home - custom */

	/* PROGRESS 
		1. complete
		2. Add new icon grid. (Incomplete)
			Set up front-page.php - to add the icon grid template seperate to page-home.php
			Created home-custom.php ACF fields for front-page.php
	*/


	/* 
	* Requirements
	* 1. Top nav and banner Please make CONTACT stand out more (maybe a black box around it?)
	*	Make the contact white text reversed out of black rectangle. 
	*	Plus Increase size of phone number at top left.
	*		
	* 2. Under tiles section, please add new icons grid 
	*	[Shan: ICONS won’t be ready yet… We are still finishing that off, Gabriel]
	*	[https://www.ardenhomes.com.au/buildguide/       
	*	When you scroll down, you will see a section titled WHAT SETS US APART. 
	*	The below icons are to appear similar to the WHAT SETS US APART grid. 
	*	Please make sure that this will be responsive in mobile environment.
	*
	* Add CTA banner 
	* - Chat with a New Homes Consultant to learn how we can help build your dream home. text - ENQUIRE NOW
	* 	Same style as the STAY UPDATED strip, but this CTA strip is stone colour with black text. 
	*	ENQUIRE NOW button prominent in black with white text]
	*		
	* Add testimonial feed Same as landing page. 
	* If we can’t add a feed, can we create as an image?
	* [Shan: https://www.ardenhomes.com.au/buildguideCan you pull this feed from the Landing page above? ]
	*		
	* Add build guide download (not gated) - not critical - also already in the tile
	*
	*/
	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromOptions('carousel_home');
	include('module/carousel/carousel.php');

	// TESTIMONIALS
	include('module/testimonials.php');

	// PANEL LINKS - Content Panel
	$module = FeatureGroup::FromPageWithPre('panel-links');
	include('module/layout/feature-group.php');

	// FOOTER
	get_footer();

?>
