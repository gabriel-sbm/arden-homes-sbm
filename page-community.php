<?php
	/* Template Name: Page - Community */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('ph-carousel-panel', 'slide');
	$carousel->navigation = 1;
	$carousel->indicator = 'none';
	include('module/carousel/carousel.php');

	// Content Panel
	$panel_class = "";
	$panel_heading = get_field('community_heading');
	$panel_intro   = get_field('community_introduction');
	$panel_content = get_field('community_content');
	$panel_button  = get_field('community_button_text');
	$panel_link    = get_field('community_button_link');
	include("module/content-panel.php");

	// Features
	$features =	FeatureItem::FromRepeaterObject(get_field('sponsors'), 'sponsor');
	FeatureItem::OutputFeaturesWithId('sponsors',$features);

	// Footer
	get_footer();

?>