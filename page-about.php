<?php
	/* Template Name: Page - About */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('ah-carousel-panel', 'slide');
	$carousel->navigation = 1;
	$carousel->indicator = 'none';
	include('module/carousel/carousel.php');

	// Content Panel
	$panel_class = "";
	$panel_heading = get_field('about_page_heading');
	$panel_intro   = get_field('about_page_introduction');
	$panel_content = get_field('about_page_content');
	include("module/content-panel.php");

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('ac-carousel-panel', 'slide');
	$carousel->navigation = 1;
	$carousel->indicator = 'none';
	include('module/carousel/carousel.php');

	// Content Panel
	$panel_class = "second-block";
	$panel_heading = get_field('about-second_page_heading');
	$panel_intro   = null;
	$panel_content = get_field('about-second_page_content');
	include("module/content-panel.php");

	// Footer
	get_footer();

?>