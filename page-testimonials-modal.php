<?php

    /* Template Name: Page - Testimonials */

  // Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPage();
	include('module/carousel/carousel.php');

	if (have_posts()) {
		while (have_posts()) {
			the_post();
?>
		<div class="content blog-post">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p><?=get_the_author()?> - <?=get_the_date()?></p>
                        
                        <?php

                        // check if the repeater field has rows of data
                        if( have_rows('videos_repeater') ):
                            $video_number = '0';
                            // loop through the rows of data
                            while ( have_rows('videos_repeater') ) : the_row();
                                ?>
                                <div class="row inner-row">
                                    <div class="col-sm-6">
                                        <!-- THE THUMBNAIL -->
                                        <img id="<?php echo($video_number); ?>" src="<?php the_sub_field('video_thumbnail'); ?>" type="button" class="btn btn-primary open-popup" data-toggle="modal" data-target=".bd-example-modal-lg<?php echo($video_number); ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <!-- THE DESCRIPTION & TITLE -->
                                        <h2><?php the_sub_field('video_title'); ?></h2>
                                        <p><?php the_sub_field('video_desc'); ?></p>
                                    </div>
                                    <!-- VIDEO MODEL -->
                                    <div id="video-player" class="modal fade bd-example-modal-lg<?php echo($video_number); ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div id="player"></div> 
                                                <iframe width="1280" height="720" src="https://www.youtube.com/embed/<?php the_sub_field('video_id') ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                                                      
                                            </div>
                                        </div>
                                    </div>
                                    <!-- VIDEO MODEL -->
                                </div>
                                    
                                <?php
                                $video_number++;
                            endwhile;

                        else :
                            // no rows found

                        endif;

                        ?>
						<?php
							the_content();

						?>  

						<p>
                            <a class="btn btn-secondary btn-social" target="_blank" href="http://www.facebook.com/sharer.php?u=<?=get_the_permalink()?>"><span class="p4-facebook"></span>Share</a>
                            <a class="btn btn-secondary btn-social" target="_blank" href="https://twitter.com/intent/tweet?text=A%20United%20Front%20with%20Colorbond%20&url=<?=get_the_permalink()?>"><span class="p4-twitter"></span>Tweet</a>
						</p>

					</div>
				</div>
			</div>
		</div>
	<?php
		}
	}
	?>

	</div>

<?php get_footer(); ?>
