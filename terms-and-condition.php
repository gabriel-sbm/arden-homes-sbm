<?php
	/* Template Name: Terms and Condition */

	get_header(); ?>
	<div class="content default-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
                        <h1><?php the_title();?></h1> 
    <?php while ( have_posts() ) : the_post(); ?>
            <div>
                 <p style="font-size:2rem !important;"><?php the_content();?></p>
                 </div>
             <?php
            endwhile; 
            wp_reset_query(); 
            ?>
                    </div>
                </div>
            </div>
    </div>
    
    
    <?php
	get_footer();
    ?>