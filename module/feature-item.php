		<li class="col-12 col-lg-<?=$feature->columns?>" style="background-color:<?=$feature->colour?>">
			<a href="<?=$feature->link?>">
				<div class="feature-image" style="background-image:url(<?=$feature->image->url?>);"></div>
				<div class="feature-content">
					<h2><?=$feature->heading?></h2>
					<?=$feature->excerpt?>
					<p class=\"feature-button\"><?=$feature->button_text?></p>
				</div>
			</a>
		</li>
