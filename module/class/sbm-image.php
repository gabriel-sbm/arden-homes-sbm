<?php
	class SBMImage {
		public $width;
		public $height;
		public $url;
		public $alt;
		public $image;

		public function __construct() {
			$this->width         = null;
			$this->height        = null;
			$this->url           = null;
			$this->alt           = null;
			$this->image         = null;
		}

		public static function ValidateImage($img, $sizes=['w'=>1200,'h'=>800]) {
			$instance            = new self();
			if (is_null($sizes) || empty($sizes)) {
				$sizes=['w'=>1200,'h'=>800];
			}
			$instance->width     = $sizes['w'];
			$instance->height    = $sizes['h'];

			$instance->url       = "https://ph4se.com.au/app/image/?text=Placeholder%20{$instance->width}%20x%20{$instance->height}px&w={$instance->width}&h={$instance->height}";
			$instance->alt       = "Placeholder {$instance->width} x {$instance->height}px";

			if (!is_null($img) && !empty($img)) {
				if (is_array($img)) {
					if (!is_null($img['url']) && !empty($img['url'])) {
						$instance->url = $img['url'];
						$instance->alt = $img['alt'];
					}
				} else {
					$instance->url   = $img;
					$instance->alt   = "";
				}
			}
			$instance->image     = "<img src=\"{$instance->url}\" alt=\"{$instance->alt}\">";
			return $instance;
		}

	}
?>