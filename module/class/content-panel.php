<?php
	class ContentPanel extends BOBject {

		public $content_style;
		public $intro;
		public $show_image;

		public function __construct() {
			parent::__construct();
			$this->content_style  = null;
			$this->intro          = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$module                    = "cp";
			$instance                  = parent::FromPageWithPre($pre, $module);
			$instance->content_style   = get_field("{$pre}_{$module}_content_style");
		  $instance->intro           = get_field("{$pre}_{$module}_intro_paragraph");
		  $instance->show_image      = get_field("{$pre}_{$module}_show_image");
		  $instance->show_image      = is_array($instance->show_image);
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$module                    = "cp";
			$instance                  = parent::FromPageIdWithPre($pre, $id, $module);
			$instance->content_style   = get_field("{$pre}_{$module}_content_style", $id);
		  $instance->intro           = get_field("{$pre}_{$module}_intro_paragraph", $id);
		  $instance->show_image      = get_field("{$pre}_{$module}_show_image", $id);
		  $instance->show_image      = is_array($instance->show_image);
			return $instance;
		}

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="cp") {
			return ContentPanel::FromPageIdWithPre($pre, "option", $module);
		}

	}
?>