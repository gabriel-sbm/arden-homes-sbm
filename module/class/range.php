<?php
	class Range {
		public $name;
		public $enabled;
		public $colour;
		public $logo;
		public $inclusions;
		public $feature_content;
		public $feature_button_text;
		public $archive_heading;
		public $archive_content;
		public $archive_image;
		public $archive_disclaimer;
		public $seo_title;
		public $seo_desc;

		public function __construct() {
			$this->name                             = "";
			$this->enabled                          = true;
			$this->colour                           = null;
			$this->logo                             = null;
			$this->inclusions                       = null;
			$this->feature_content                  = "";
			$this->feature_button_text              = "VIEW RANGE";
			$this->archive_heading                  = "";
			$this->archive_content                  = "";
			$this->archive_image                    = null;
			$this->archive_disclaimer               = null;
			$this->seo_title                        = null;
			$this->seo_desc                         = null;
		}

		// RANGE OBJECT
		//
		public static function GetRange($target) {
			$instance                               = new self();
			$ranges                                 = $instance::GetRangesData();
			if (is_array($ranges)) {
				if (count($ranges) > 0) {
					foreach ($ranges as $range) {
						if ($range['range_name'] == $target) {
							$instance->name                   = $range['range_name'];
							$instance->enabled                = $range['enable_range'];
							$instance->colour                 = $range['range_colour'];
							$instance->logo                   = str_replace(' ','-',strtolower($instance->name));
							$instance->inclusions             = $range['range_inclusions_pdf'];
							$instance->feature_content        = $range['ranges_feature_content'];
							if (!empty($range['ranges_feature_button_text'])) {
								$instance->feature_button_text  = $range['ranges_feature_button_text'];
							}
							$instance->archive_heading        = $range['ranges_archive_heading'];
							$instance->archive_content        = $range['ranges_archive_content'];
							$instance->archive_image          = SBMImage::ValidateImage($range['ranges_archive_image'],['w'=>1600,'h'=>500]);
							$instance->archive_disclaimer     = $range['ranges_archive_disclaimer'];
							$instance->seo_title              = $range['range_seo_title'];
							$instance->seo_desc               = $range['range_seo_description'];
						}
					}
				}
			}
			return $instance;
		}

		// ALL RANGES
		//
		public static function GetRanges() {
			$all_ranges                             = [];
			$instance                               = new self();
			$ranges                                 = $instance::GetRangesData();
			if (count($ranges) > 0) {
				foreach ($ranges as $range) {
					$all_ranges[]                       = Range::GetRange($range['range_name']);
				}
			}
			return $all_ranges;
		}

		// ALL RANGES DATA
		//
		private function GetRangesData() {
			return get_field('ranges', 'options');
		}


	}
?>