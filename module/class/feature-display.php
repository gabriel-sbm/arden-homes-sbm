<?php
	class FeatureDisplay {

		public $id;
		public $name;
		public $address;
		public $coordinates;
		public $opening_hours;
		public $contact;
		public $contacts;
		public $time_and_day;
		public $map;
		public $colour;
		public $columns;
		public $image;
		public $link;
		public $displays;

		public function __construct() {
			$this->id                     = null;
			$this->name                   = "";
			$this->address                = "";
			$this->coordinates            = "";
			$this->opening_hours          = "";
			$this->contact                = null;
			$this->contacts               = [];
			$this->time_and_day           = [];
			$this->map                    = "";
			$this->colour                 = null;
			$this->columns                = 4;
			$this->image                  = null;
			$this->link                   = "https://www.google.com/maps/search/?api=1&query=";
			$this->displays               = [];
		}

		public static function FromObject($obj, $pre="") {
			$instance                     = new self();
			$instance->contact            = Contact::FromObject($obj, $pre);
			$pre = (!is_null($pre) && !empty($pre))?"{$pre}_":"";

			$contacts                     = $obj["{$pre}contact_repeater"];
			foreach ($contacts as $contact) {
				$instance->contacts[]       = Contact::FromObject($contact, "");
			}

			$instance->name               = $obj["{$pre}name"];
			if (!empty($instance->name))
				$instance->id               = str_replace(' ', '-', strtolower($instance->name));
			$instance->address            = $obj["{$pre}address"];
			$instance->coordinates        = $obj["{$pre}coordinates"];
			$instance->link              .= str_replace(" ", "+", $instance->coordinates['address']);
			$instance->opening_hours      = $obj["{$pre}contact_email"];
			$instance->image              = SBMImage::ValidateImage($obj["{$pre}feature_image"],['w'=>1600,'h'=>500]);
			$instance->map                = SBMImage::ValidateImage($obj["{$pre}map"],['w'=>1200,'h'=>600]);
			$opening_hours                = $obj["{$pre}opening_hours"];
			if (!is_null($opening_hours) && !empty($opening_hours) && count($opening_hours) > 0) {
				foreach ($opening_hours as $time_day) {
					$instance->time_and_day[] = $time_day['time_and_day'];
				}
			}
			$display_homes                = $obj["{$pre}home_types"];
			if (!is_null($display_homes) && !empty($display_homes) && count($display_homes) > 0) {
				foreach ($display_homes as $display_home) {
					$instance->displays[]    = $display_home['display_home_name'];
				}
			}
			$instance->colour             = get_field("{$pre}feature_colour");
			$instance->columns            = get_field("{$pre}feature_columns");
			return $instance;
		}

		public static function FromRepeaterObject($objs, $pre) {
			$features = [];
			if (!is_null($objs) && !empty($objs) && count($objs) > 0) {
				foreach ($objs as $obj) {
					$features[] = FeatureDisplay::FromObject($obj, $pre);
				}
			}
			return $features;
		}

		public function OutputFeature() {
			echo "
				<li id=\"{$this->id}\" class=\"col-12 col-lg-{$this->columns}\" style=\"background-color:{$this->colour}\">
					<div class=\"feature-block\">
						<div class=\"feature-image\" style=\"background-image:url({$this->image->url});\"></div>
						<div class=\"feature-content\">
							<h2>{$this->name}</h2>
							<p class=\"feature-address\">{$this->address}</p>
							<p><a href=\"{$this->link}\" class=\"google-map\" target=\"_blank\">GET DIRECTIONS</a></p>
							<div class=\"feature-contact\">
								<h3>Contact Details</h3>
								<p>
									{$this->contact->name},
			";
			if (count($this->contacts) > 1) {
				echo "    <br>
						      {$this->contact->position}<br>
				";
			} else 
			if (count($this->contacts) > 0) {
				echo "    {$this->contact->position}<br>";
      }
      // Contacts
      foreach ($this->contacts as $contact) {
      	echo "
									{$contact->name}: <strong>{$contact->mobile}</strong><br>
									{$contact->email}<br>
				";
      }
      // Landline
			echo "
									Land line: <strong>{$this->contact->land_line}</strong><br>
			";

			if (!is_null($this->time_and_day) && !empty($this->time_and_day) && count($this->time_and_day)>0) {
				foreach ($this->time_and_day as $time_day) {
					echo   "{$time_day}<br>";
				}
			}
			echo "    </p>";

			// Home Displays
			//
			if (is_array($this->displays) && count($this->displays) > 0) {
				echo "
								<h3>Homes on Display</h3>
								<p>
									".implode(', ', $this->displays)."
								</p>
				";
			}

			if (!(strpos($this->map->url, "Placeholder") > 0)) {
				echo "
								<p>
									<a href=\"{$this->map->url}\" target=\"_block\">VIEW DISPLAY MAP</a>
								</p>
				";
			}
			echo "
							</div>
						</div>
					</div>
				</li>
			";
		}

		public function OutputFeaturesWithClass($class, $features) {
			if (count($features) > 0) {
				echo "
					<section class=\"feature-range {$class}\">
						<div class=\"container\">
							<ul class=\"row\">
				";
				foreach ($features as $feature) {
					$feature->OutputFeature();
				}
				echo "
							</ul>
						</div>
					</section>
				";
			}
		}


	}
?>