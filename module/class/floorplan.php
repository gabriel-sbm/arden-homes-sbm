<?php
	include_once('layout.php');

	class Floorplan {
		public $layouts;

		public function __construct() {
			$this->layouts           = [];
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($home, $pre="fp") {
			$pre                     = (!is_null($pre) && !empty($pre))?"{$pre}_":"";
			$instance                = new self();
			$layouts                 = get_field($pre."layouts");
			foreach ($layouts as $layout) {
				$instance->layouts[]   = Layout::FromRepeater($home, $layout);
			}
			return $instance;
		}

	}
?>