
<?php
	class Layout {
		public $home;
		public $name;
		public $levels;
		public $download;
		public $dimensions;
		public $options;

		public function __construct($home=null) {
			$this->home                    = $home;
			$this->name                    = "";
		  $this->levels                  = [];
		  $this->download                = null;
		  $this->dimensions              = [];
		  $this->options                 = [];
		}

		// FROM PAGE WITH PRE
		//
		public static function FromRepeater($home, $obj) {
			$instance                      = new self($home);
			$instance->name                = $obj['fp_layout_name'];
			
			$levels                        = $obj['fp_levels']; 
			if (is_array($levels) && count($levels) > 0) {
				foreach ($levels as $level) {
					$new_slide                   = new Slide();
					$new_slide->image            = SBMImage::ValidateImage($level['fp_level_image'],['w'=>1600,'h'=>600]);
					$new_slide->title            = "{$home} {$instance->name}";
					if (count($levels) > 1 && !empty($level['fp_level_name'])) {
						$new_slide->title       .= " | {$level['fp_level_name']}";
					}
					$instance->levels[]          = $new_slide;
				}
			}
			$instance->download            = $obj['fp_download_pdf'];

			$dimensions                    = $obj['fp-dimensions'];
			foreach ($dimensions as $dimension) {
				$instance->dimensions[]      = [
					'label'                    => $dimension['fp_dimension_label'],
					'size'                     => str_replace("mxm", "m<sup>2</sup>", $dimension['fp_dimension_size']),
				];
			}

			$options                       = $obj['fp_options'];
			if (!is_null($options) && !empty($options) && count($options) > 0) {
				foreach ($options as $option) {
					$new_slide                   = new Slide();
					$new_slide->image            = SBMImage::ValidateImage($option['fp_option_image'],['w'=>1200,'h'=>800]);
					$instance->options[]         = $new_slide;
				}
			}

			return $instance;
		}
	}
?>