<?php
	class Carousel {

		public $id;
		public $height;
		public $animation;
		public $indicator;
		public $navigation;
		public $bgcolour;
		public $captions;
		public $delay;
		public $use_placeholders;
		public $placeholders;
		public $slides;
		public $parent;

		public function __construct($id=null) {
			$this->id               = $id;
		  $this->height           = '500';
			$this->animation        = 'slide';		// 'slide'=>'Slide', 'fader'=>'Fade'
			$this->indicator        = 'none';			// 'flat'=>'Flat', 'square'=>'Square', 'circle'=>'Circle', 'ring'=>'Ring', 'none'=>'None',
			$this->navigation       = 0;					// 1=>'Yes' || 0=>'No'
			$this->bgcolour         = '#767676';
			$this->captions         = 0;					// 1=>'Yes' || 0=>'No'
			$this->delay            = 4000;
			$this->use_placeholders = null;
			$this->placeholders     = [];
			$this->slides           = [];
			$this->panel            = null;
		}

		// CAROUSEL FROM OPTIONS
		//
		public static function FromOptions($id=null) {
			$instance                   = new self($id);
			$instance->height           = get_field('carousel_height','options');
			$instance->animation        = get_field('animation_style','options');
			$instance->indicator        = get_field('indicator_style','options');
			$instance->navigation       = get_field('navigation_arrows','options');
			$instance->bgcolour         = get_field('carousel_background_colour','options');
			$instance->captions         = get_field('show_captions','options');
			$instance->delay            = get_field('animation_delay','options') * 1000;
			$instance->use_placeholders = is_array(get_field('use_placeholders','options'));
			$slides                     = get_field('slides','options');
			$instance->slides           = $instance::GetSlidesFromRepeater($slides);
			$placeholders               = get_field('placeholders','options');
			if (is_array($placeholders) && count($placeholders) > 0 && !empty($placeholders[0])) {
				foreach ($placeholders as $placeholder) {
					$instance->placeholders[] = $placeholder['image_urls'];
				}
			} else {
				$instance->placeholders[] = [ "url" => "https://ph4se.com.au/app/image/?text=Slide%201&w=2000&h=750&col1=FFF&col2=755&size=40"];
				$instance->placeholders[] = [ "url" => "https://ph4se.com.au/app/image/?text=Slide%202&w=2000&h=750&col1=FFF&col2=575&size=40"];
				$instance->placeholders[] = [ "url" => "https://ph4se.com.au/app/image/?text=Slide%203&w=2000&h=750&col1=FFF&col2=557&size=40"];
			}
			return $instance;
		}
		// SLIDES FROM OPTIONS
		//
		private function GetSlidesFromRepeater($slides, $pre=null) {
			$new_slides = [];
			if (!empty($slides) && count($slides) > 0) {
				foreach ($slides as $slide) {
					$new_slides[] = Slide::FromOptionRowWithPre($slide, $pre);
				}
				return $new_slides;
			}		
			return [];	
		}

		// TESTIMONIALS FROM PAGE WITH PRE
		//
		public static function TestimonialsWithPre($pre="module", $module="slide") {
			$instance                   = new self();
			$instance->panel            = BOBject::FromPageWithPre($pre, $module);
			return $instance;
		}

		// WITH FEATURE IMAGE
		//
		public static function FromPage() {
			$instance                   = new self();
			$instance->captions         = 1;
			$slide                      = Slide::FromPost();
			if (empty($slide->title)) {
				$slide->title             = get_the_title();
			}
			$instance->slides[]         = $slide;
			return $instance;
		}

		// CAROUSEL FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="slide") {
			$instance                   = new self();
			$instance->id               = $pre;
			$pre = (!is_null($pre) && !empty($pre))?"{$pre}_":"";
			$instance->height           = get_field("{$pre}carousel_height");
			$instance->animation        = get_field("{$pre}animation_style");
			$instance->indicator        = get_field("{$pre}indicator_style");
			$instance->navigation       = get_field("{$pre}navigation_arrows");
			$instance->bgcolour         = get_field("{$pre}carousel_background_colour");
			$instance->captions         = 1; //get_field("{$pre}show_captions");
			$instance->delay            = get_field("{$pre}animation_delay") * 1000;
			$slides                     = get_field("{$pre}slides");
			$instance->slides           = $instance::GetSlidesFromRepeater($slides, $pre);
			$placeholders               = get_field('placeholders','options');
			if (is_array($placeholders) && count($placeholders) > 0 && !empty($placeholders[0])) {
				foreach ($placeholders as $placeholder) {
					$instance->placeholders[] = $placeholder['image_urls'];
				}
			} else {
				$instance->placeholders[] = [ "url" => "https://ph4se.com.au/app/image/?text=Slide%201&w=2000&h=750&col1=FFF&col2=755&size=40"];
				$instance->placeholders[] = [ "url" => "https://ph4se.com.au/app/image/?text=Slide%202&w=2000&h=750&col1=FFF&col2=575&size=40"];
				$instance->placeholders[] = [ "url" => "https://ph4se.com.au/app/image/?text=Slide%203&w=2000&h=750&col1=FFF&col2=557&size=40"];
			}
			return $instance;
		}



	}
?>