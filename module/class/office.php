<?php
	class Office {
		public $office_name;
		public $primary;
		public $show_marker;
		public $phone;
		public $email;
		public $address;

		public function __construct() {
			$this->office_name         = null;
			$this->primary             = false;
			$this->show_marker         = false;
			$this->phone               = null;
			$this->email               = null;
			$this->address             = null;
		}

		// GET GROUP FROM OPTIONS
		//
		public static function GetFieldsFromOptions() {
			$offices                   = get_field('locations','options');
			$office_group              = [];
			foreach ($offices as $office) {
				$instance                = new self();
				$instance->office_name   = $office['office_name'];
				$instance->primary       = $office['primary_location'];
				$instance->show_marker   = $office['show_marker'];
				$instance->phone         = $office['phone'];
				$instance->email         = $office['email'];
				$instance->address       = $office['address'];
				$office_group[]          = $instance;
			}
			return $office_group;
		}

		private function WrapTitle($str, $tag) {
			if (!empty($str)) {
				if ($tag == "module_title") {
					$str = "<div class=\"$tag\">{$str}</div>";
				} else {
					$str = "<{$tag}>{$str}</{$tag}>";
				}
			}
			return $str;
		}

	}
?>