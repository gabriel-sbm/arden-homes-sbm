<?php
	class CallToActionExpand extends BOBject {
		public $hidden_content;
		public $button_text_closed;
		public $button_text_open;
		public $button_style;

		public function __construct() {
			parent::__construct();
			$this->hidden_content         = null;
		  $this->button_text_closed     = null;
		  $this->button_text_open       = null;
		  $this->button_style           = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="ctae") {
			$instance                     = parent::FromPageWithPre($pre, $module);
			$instance->hidden_content     = get_field("{$pre}_{$module}_hidden_content");
		  $instance->button_text_closed = get_field("{$pre}_{$module}_button_text_closed");
		  $instance->button_text_open   = get_field("{$pre}_{$module}_button_text_open");
		  $instance->button_style       = get_field("{$pre}_{$module}_button_style");
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="ctae") {
			$instance                     = parent::FromPageIdWithPre($pre, $id, $module);
			$instance->hidden_content     = get_field("{$pre}_{$module}_hidden_content", $id);
		  $instance->button_text_closed = get_field("{$pre}_{$module}_button_text_closed", $id);
		  $instance->button_text_open   = get_field("{$pre}_{$module}_button_text_open", $id);
		  $instance->button_style       = get_field("{$pre}_{$module}_button_style", $id);
			return $instance;
		} // panel-6_ctae_hidden-content

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="ctae") {
			return CallToAction::FromPageIdWithPre($pre, "option", $module);
		}

	}
?>