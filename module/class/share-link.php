<?php
	class ShareLink {
		public $share;
		public $title;
		public $username;
		public $subject;
		public $content;

		public $permalink;
		public $template;
		public $link;

		public function __construct() {
			$this->share        = null;
			$this->title        = null;
			$this->username     = null;
			$this->subject      = null;
			$this->content      = null;

		  $this->permalink    = null;
		  $this->template     = null;
		  $this->link         = null;
		}

		// FROM OBJECT WITH PRE
		//
		public static function FromObjectWithPre($obj, $pre) {
			$instance           = new self();
			$instance->share    = $obj["{$pre}_sl_share"];
			$instance->title    = $obj["{$pre}_sl_share-title"];
			$instance->username = $obj["{$pre}_sl_username"];
			$instance->subject  = $obj["{$pre}_sl_subject"];
			$instance->content  = $obj["{$pre}_sl_content"];
			return $instance;
		}

	}
?>