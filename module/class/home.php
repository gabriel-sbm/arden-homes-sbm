<?php
	include_once('floorplan.php');

	class Home {
		public $title;
		public $feature_heading;
		public $feature_button_text;
		public $feature_image;
		public $price;
		public $excerpt;
		public $description;
		public $features; // Rooms, Bathrooms etc.
		public $link;
		public $hero_caption;
		public $hero_image;
		public $awards;
		public $gallery_facade_slides;
		public $gallery_facade_use_group;
		public $gallery_facade_group;
		public $gallery_interior_slides;
		public $inclusion_heading;
		public $inclusion_caption;
		public $inclusion_image;
		public $inclusion_description;
		public $inclusion_button_text;
		public $inclusion_file;
		public $floorplan;
		public $promotion_show;
		public $promotion_heading;
		public $promotion_caption;
		public $promotion_image;
		public $promotion_description;
		public $promotion_button_text;
		public $promotion_file;

		public function __construct() {
			$this->title                         = "";
			$this->feature_heading               = "";
			$this->feature_button_text           = "";
			$this->feature_image                 = "";
			$this->price                         = null;
			$this->excerpt                       = "";
			$this->description                   = "";
			$this->features                      = [];
			$this->link                          = null;
			$this->hero_caption                  = "";
			$this->hero_image                    = null;
			$this->awards                        = [];
			$this->gallery_facade_slides         = [];
			$this->gallery_facade_use_group      = false;
			$this->gallery_facade_group          = "";
			$this->gallery_interior              = [];
			$this->floorplan                     = null;
			$this->inclusion_heading             = "";
			$this->inclusion_caption             = "";
			$this->inclusion_image               = null;
			$this->inclusion_description         = "";
			$this->inclusion_button_text         = "";
			$this->inclusion_file                = "";
			$this->promotion_show                = true;
			$this->promotion_heading             = "";
			$this->promotion_caption             = "";
			$this->promotion_image               = null;
			$this->promotion_description         = "";
			$this->promotion_button_text         = "";
			$this->promotion_file                = "";
		}

		// Home
		//
		public static function FromPage() {
			$instance = Home::FeatureFromPage();
			$instance->description               = get_field('home_description');
			$instance->hero_caption              = get_field('home_hero_caption');
			$instance->hero_image                = SBMImage::ValidateImage(get_field('home_hero_image'),['w'=>1600,'h'=>600]);
			$instance->inclusion_heading         = get_field('inclusion_heading');
			$instance->inclusion_caption         = get_field('inclusion_hero_caption');
			$instance->inclusion_image           = SBMImage::ValidateImage(get_field('inclusion_hero_image'),['w'=>1600,'h'=>600]);
			$instance->inclusion_description     = get_field('inclusion_description');
			$instance->inclusion_button_text     = get_field('inclusion_button_text');
			$instance->inclusion_file            = get_field('inclusion_file');
			$instance->promotion_show            = get_field('use_promotion');
			$instance->promotion_heading         = get_field('promotion_heading');
			$instance->promotion_caption         = get_field('promotion_hero_caption');
			$instance->promotion_image           = SBMImage::ValidateImage(get_field('promotion_hero_image'),['w'=>1600,'h'=>600]);
			$instance->promotion_description     = get_field('promotion_description');
			$instance->promotion_button_text     = get_field('promotion_button_text');
			$instance->promotion_file            = get_field('promotion_file');

			// Features
			$home_features                       = get_field('home_features');
			if (!is_null($home_features) && !empty($home_features) && count($home_features) > 0) {
				foreach ($home_features as $feature) {
					$instance->features[]            = [
						'type'   => $feature['feature_type'],
						'amount' => $feature['feature_amount']
					];
				}
			}
			// Home Size and Floor Plans
			$instance->floorplan                 = Floorplan::FromPageWithPre($instance->title, 'fp');
			// Awards
			$home_awards                         = get_field('awards_repeater');
			if (!is_null($home_awards) && !empty($home_awards) && count($home_awards) > 0) {
				$instance->awards = $home_awards;
			}
			// Gallery Facades
			$instance->gallery_facade_use_group  = get_field('use_slide_group');
			$instance->gallery_facade_group      = get_field('slide_group_name');
			if ($instance->gallery_facade_use_group) {
				// Use Slide Group
				$slides = get_field('slide_groups','options');
				foreach ($slides as $slide_group) {
					if ($slide_group['slide_name'] == $instance->gallery_facade_group) {
						foreach ($slide_group['slide_group'] as $images) {
							$new_slide = new Slide();
							$new_slide->image = SBMImage::ValidateImage($images['slide_image'],['w'=>1600,'h'=>600]);
							$instance->gallery_facade_slides[] = $new_slide;
						}
					}
				}
			} else {
				// User Defined Slides
				$gallery_facades = get_field('facades_gallery');
				if (!is_null($gallery_facades) && !empty($gallery_facades) && count($gallery_facades) > 0) {
					foreach ($gallery_facades as $facade) {
						$new_slide = new Slide();
						$new_slide->image = SBMImage::ValidateImage($facade['image'],['w'=>1600,'h'=>600]);
						$instance->gallery_facade_slides[] = $new_slide;
					}
				}
			} 
			// Gallery Interior
			$gallery_interior = get_field('interiors_gallery');
			if (!is_null($gallery_interior) && !empty($gallery_interior) && count($gallery_interior) > 0) {
				foreach ($gallery_interior as $interior) {
					$new_slide = new Slide();
					$new_slide->image = SBMImage::ValidateImage($interior['image'],['w'=>1600,'h'=>600]);
					$instance->gallery_interior_slides[] = $new_slide;
				}
			}

			return $instance;
		}

		// FEATURE
		//
		public static function FeatureFromPage($pre="") {
			$pre = (!is_null($pre) && !empty($pre))?"{$pre}_":"";
			$instance                            = new self();
			$instance->title                     = get_the_title();
			if (!empty(get_field('home_name'))) {
				$instance->title                   = get_field('home_name');
			}
			$instance->feature_heading           = get_field("{$pre}home_feature_heading");
			$instance->feature_button_text       = get_field("{$pre}home_feature_button_text");
			$instance->feature_image             = SBMImage::ValidateImage(get_field("{$pre}home_feature_image"),['w'=>1600,'h'=>500]);
			$instance->price                     = get_field("{$pre}home_feature_price");
			if (is_null($instance->price) || empty($instance->price)) {
				$instance->price = null;
			}
			$instance->excerpt                   = get_field("{$pre}home_feature_excerpt");
			$instance->link                      = get_permalink();
			return $instance;
		}

		// OUTPUT FEATURE
		//
		public function OutputFeature($range) {
			$escape = (strtolower($range->name) == "escape");
			echo "
				<li class=\"col-12 col-lg-4\" style=\"background-color:{$range->colour};\">
					<a href=\"{$this->link}\">
						<div class=\"feature-image\" style=\"background-image:url({$this->feature_image->url});\"></div>
						<div class=\"feature-content\">
							<h2>{$this->feature_heading}</h2>
			";
			if (!is_null($this->price)) { // && !$escape) {
				echo "
							<p class=\"feature-price\">Priced from {$this->price}</p>
				";
			}
			echo "
							{$this->excerpt}
							<p class=\"feature-button\">".($escape?$this->feature_button_text:$this->feature_button_text)."</p>
						</div>
					</a>
				</li>
			";
		}


	}
?>