<?php
	class FeatureItem {
		public $heading;
		public $excerpt;
		public $button_text;
		public $link;
		public $colour;
		public $columns;
		public $image;

		public function __construct() {
			$this->heading            = "";
			$this->excerpt            = null;
		  $this->button_text        = "";
			$this->link               = "#";
			$this->colour             = null;
			$this->columns            = 4;
			$this->image              = null;
		}

		public static function FromRepeater($obj, $pre="") {
			$instance                 = new self();
			$instance->heading        = $obj["{$pre}feature_heading"];
			$instance->excerpt        = $obj["{$pre}feature_excerpt"];;
		  $instance->button_text    = $obj["{$pre}feature_button_text"];
		  if (isset($obj["{$pre}feature_link"]['url'])) {
			  $instance->link         = $obj["{$pre}feature_link"]['url'];
			} else {
			  $instance->link         = $obj["{$pre}feature_link"];
			}
			$instance->colour         = $obj["{$pre}feature_colour"];
			$instance->columns        = $obj["{$pre}feature_columns"];
			$instance->image          = SBMImage::ValidateImage($obj["{$pre}feature_image"],['w'=>1600,'h'=>500]);
			return $instance;
		}

		public static function FromRepeaterObject($objs, $pre) {
			$features = [];
			if (!is_null($objs) && !empty($objs) && count($objs) > 0) {
				$pre = (!is_null($pre) && !empty($pre))?"{$pre}_":"";
				foreach ($objs as $obj) {
					$features[] = FeatureItem::FromRepeater($obj, $pre);
				}
			}
			return $features;
		}

		public function OutputFeature() {
			$link_start  = "<div class=\"feature-block\">";
			$link_finish = "</div>";
			$button = "";
			if (!is_null($this->link) && !empty($this->link)) {
				$link_start  = "<a href=\"{$this->link}\" target=\"_blank\">";
				$link_finish = "</a>";
				if (!is_null($this->button_text) && !empty($this->button_text)) {
					$button = "<p class=\"btn btn-secondary\">{$this->button_text}</p>";
				}
			}
			echo "
				<li class=\"col-12 col-lg-{$this->columns}\" style=\"background-color:{$this->colour}\">
					{$link_start}
						<div class=\"feature-image\" style=\"background-image:url({$this->image->url});\"></div>
						<div class=\"feature-content\">
							<h2>{$this->heading}</h2>
							{$this->excerpt}
							{$button}
						</div>
					{$link_finish}
				</li>
			";
		}

		public function OutputFeaturesWithId($id, $features) {
			if (count($features) > 0) {
				echo "
					<section class=\"feature-range {$id}\">
						<div class=\"container\">
							<ul class=\"row\">
				";
				foreach ($features as $feature) {
					$feature->OutputFeature();
				}
				echo "
							</ul>
						</div>
					</section>
				";
			}
		}


	}
?>