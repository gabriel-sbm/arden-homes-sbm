<?php
	class SBMObject {

		public $id;
		public $title;
		public $slug;
		public $content;
		public $permalink;

		public function __construct() {
			$this->id         = null;
			$this->title      = null;
			$this->slug       = null;
			$this->content    = null;
			$this->permalink  = null;
		}

		// FROM POST
		//
		public static function FromPost() {
			$instance            = new self();
			$instance->id        = get_the_id();
			$instance->title     = get_field('page_title');
			if (is_null($instance->title) || empty($instance->title)) {
				$instance->title   = get_the_title();
			}
			$instance->content   = get_field('content');
			return $instance;
		}

		private function ValidateImage($obj, $size) {
			if (!isset($obj['url'])) {
				return validateImage([
					'url' => $obj,
					'alt' => '',
				],$size);
			} else {
				return validateImage($obj, $size);
			}
		}

	}

?>

