<?php
	class Share {
		public $title_tag;
		public $title;
		public $links;

		public function __construct() {
			$this->title_tag         = "module_title";
		  $this->title             = null;
		  $this->links             = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module") {
			$instance                = new self();
			$instance->title_tag     = get_field("{$pre}_sl_title_tag");
			$instance->title         = $instance->WrapTitle(get_field("{$pre}_sl_title"), $instance->title_tag, $font_colour);
			$share_links             = get_field("{$pre}_sl_sharing");
			if (!is_null($share_links) && !empty($share_links)) {
				foreach ($share_links as $share) {
					$share_link            = ShareLink::FromObjectWithPre($share, $pre);
					$share_link->template  = $instance->GetTemplateFromShare($share_link->share);
					$share_link->permalink = get_the_permalink();
					// Create Share Link
					$share_link->link      = str_replace("##permalink##", $share_link->permalink, $share_link->template);
					$share_link->link      = str_replace("##title##", $share_link->title, $share_link->link);
					$share_link->link      = str_replace("##username##", $share_link->username, $share_link->link);
					$share_link->link      = str_replace("##subject##", $share_link->subject, $share_link->link);
					$share_link->link      = str_replace("##content##", $share_link->content, $share_link->link);
					$instance->links[] = $share_link;
				}
			}
			return $instance;
		}

		private function WrapTitle($str, $tag) {
			if (!empty($str)) {
				if ($tag == "module_title") {
					$str = "<span class=\"$tag\">{$str}</span>";
				} else {
					$str = "<{$tag}>{$str}</{$tag}>";
				}
			}
			return $str;
		}

		private function GetTemplateFromShare($share) {
			switch ($share) {
				case 'facebook':
					return "http://www.facebook.com/sharer.php?u=##permalink##";
					break;
				case 'twitter':
					return "http://twitter.com/share?url=##permalink##&text=##title##";
					break;
				case 'instagram':
					return "https://instagram.com/##username##";
					break;
				case 'google-plus':
					return "https://plus.google.com/share?url=##permalink##";
					break;
				case 'linkedin':
					return "http://www.linkedin.com/shareArticle?mini=true&url=##permalink##";
					break;
				case 'envelope':
					return "mailto:?Subject=##subject##&Body=##content## ##permalink##";
					break;
				default:
					return "";
					break;
			}
		}

	}
?>