<?php
	class Contact {
		public $name;
		public $position;
		public $mobile;
		public $land_line;
		public $email;

		public function __construct() {
			$this->name               = "";
			$this->position           = "";
			$this->mobile             = "";
			$this->land_line          = "";
			$this->email              = "";
		}

		public static function FromObject($obj, $pre="") {
			$pre = (!is_null($pre) && !empty($pre))?"{$pre}_":"";
			$instance                 = new self();
			$instance->name           = $obj["{$pre}contact_name"];
			$instance->position       = $obj["{$pre}contact_position"];
			$instance->mobile         = $obj["{$pre}contact_mobile"];
			$instance->land_line      = $obj["{$pre}contact_land_line"];
			$instance->email          = $obj["{$pre}contact_email"];
			return $instance;
		}

	}
?>