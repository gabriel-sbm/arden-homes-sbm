<?php
	class EqualSplit extends BOBject {

		public function __construct() {
			parent::__construct();
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$module               = "es";
			$instance             = parent::FromPageWithPre($pre, $module);
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$module               = "es";
			$instance             = parent::FromPageIdWithPre($pre, $id, $module);
			return $instance;
		}

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="es") {
			return EqualSplit::FromPageIdWithPre($pre, "option", $module);
		}

	}
?>