<?php
	/*
			SLIDE v1.1
			-- FromPostWithPre()
			-- Added Internal/External link switch


	*/
	class Slide {
		
		public $id;
		public $title;
		public $content;
		public $feature;
		public $image;
		public $link;
		public $internal;
		public $alignment;

		public function __construct() {
			$this->id            = null;
			$this->title         = null;
			$this->content       = null;
			$this->feature       = false;
			$this->image         = null;
			$this->link          = null;
			$this->internal      = true;
			$this->alignment     = "center";
		}

		public static function FromPost() {
			$instance            = new self();
			$instance->id        = get_the_id();
			$instance->title     = get_field('heading');
			$instance->content   = get_field('content');
			$instance->feature   = get_field('caption_feature');
			$instance->image     = SBMImage::ValidateImage(['url'=>get_the_post_thumbnail_url(),'alt'=>''],['w'=>2000,'h'=>1000]);
			$instance->link      = get_permalink();
			$align               = get_field('image_alignment');
			$instance->alignment = !empty($align)?$align:"center";
			return $instance;
		}

		public static function FromPostWithPre($pre="") {
			if (!empty($pre)) $pre .= "_";
			$instance            = new self();
			$instance->id        = get_the_id();
			$instance->title     = get_field("{$pre}caption");
			$instance->content   = get_field("{$pre}content");
			$instance->image     = SBMImage::ValidateImage(get_field("{$pre}image"),['w'=>2000,'h'=>1000]);
			$instance->internal  = get_field("{$pre}internal_link");
			if ($instance->internal) {
				$instance->link    = get_field("{$pre}page_link");
			} else {
				$instance->link    = get_field("{$pre}external_link");
			}
			return $instance;
		}

		public static function FromOptionRow($row) {
			$instance            = new self();
			$instance->id        = 'option';
			$instance->title     = $row['slide_heading'];
			$instance->content   = $row['slide_content'];
			$instance->feature   = $row['caption_feature'];
		  $instance->image     = SBMImage::ValidateImage($row['slide_image'],['w'=>1600,'h'=>500]);
			$instance->link      = $row['slide_link'];
			$align               = $row['slide_image_alignment'];
			$instance->alignment = !empty($align)?$align:"center";
			return $instance;
		}

		public static function FromOptionRowWithPre($row, $pre=null) {
			$instance            = new self();
			$instance->id        = 'option';
			$instance->title     = $row["{$pre}slide_heading"];
			$instance->content   = $row["{$pre}slide_content"];
			$instance->feature   = $row["{$pre}caption_feature"];
		  $instance->image     = SBMImage::ValidateImage($row["{$pre}slide_image"],['w'=>1600,'h'=>500]);
			$instance->internal  = ($row["{$pre}slide_internal_link"])?true:false;
			$instance->link      = $row["{$pre}slide_link"];
			if ($instance->internal == 1) {
				$instance->link      = $row["{$pre}slide_page_link"];
			}
			$align               = $row["{$pre}slide_image_alignment"];
			$instance->alignment = !empty($align)?$align:"center";
			return $instance;
		}

		public static function WithImage($image=null,$size=['w'=>1600,'h'=>500]) {
			$instance            = new self();
			if (!is_null($image) && !empty($image)) {
			  $instance->image   = SBMImage::ValidateImage($image,$size);
			}
			return $instance;
		}

	}
?>