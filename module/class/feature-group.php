<?php
	/*
			New Version!!
			- Change features to be inside an Unordered List
			- Updated 'no-link' to only appear if the link is empty
			- Button is optional in tcf layout

	*/
	class FeatureGroup extends BOBject {
		public $data_source;
		public $custom_data;
		public $is_carousel;
		public $max_features;
		public $column_layout;
		public $content_width;
		public $features;

		public function __construct() {
			parent::__construct();
			$this->data_source        = null;
			$this->custom_data        = null;
			$this->is_carousel        = false;
			$this->max_features       = -1;
			$this->column_layout      = null;
			$this->feature_style      = null;
			$this->content_width      = null;
			$this->features           = [];
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$module                   = "fg";
			$instance                 = parent::FromPageWithPre($pre, $module);
			$instance->data_source    = get_field("{$pre}_{$module}_data_source");
			$instance->custom_data    = get_field("{$pre}_{$module}_custom_data");
			$instance->is_carousel    = get_field("{$pre}_{$module}_is_carousel");
			$instance->feature_style  = get_field("{$pre}_{$module}_feature_style");
			$instance->max_features   = get_field("{$pre}_{$module}_max_features");
			$instance->column_layout  = get_field("{$pre}_{$module}_column_layout");
			$instance->content_width  = get_field("{$pre}_{$module}_content_width");
			// Get wp_query
			$item_max = $instance->max_features;
			$item_count = ($item_max > 0)?$item_max+1:$item_max;
			// Get Data frm Source
			if ($instance->data_source != "custom") {
				//
				// Data from Post type
				//
				$data = new WP_Query(
					array(
						'post_type' => $instance->data_source,
						'posts_per_page' => $item_count,
					)
				);
				// Get Features Objects
				$page_id = get_the_id();
				$display_count = 0;
				while ( $data->have_posts() ) {
					$data->the_post();
					// if ($_GET['debug']) {
						if ($item_max == -1 || $page_id != get_the_id()) {
							if ($item_max == -1 || $item_max > $display_count) {
								$feature = Feature::FromPostWithPre($instance->data_source);
								$instance->features[] = $feature;
								$display_count++;
							}
						}

					// } else {
					// 	$feature = Feature::FromPostWithPre($instance->data_source);
					// 	$instance->features[] = $feature;
					// 	$display_count++;
					// }
				}
			} else {
				//
				// Data from Repeater
				//
				$display_count = 0;
				foreach ($instance->custom_data as $new_feature) {
					if ($item_max == -1 || $item_max > $display_count) {
						$feature = Feature::FromRepeaterObjectWithPre("{$pre}_{$module}_data",$new_feature);
						$instance->features[] = $feature;
						$display_count++;
					}
				}
				
			}
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$module                   = "fg";
			$instance                 = parent::FromPageIdWithPre($pre, $id, $module);
			$instance->data_source    = get_field("{$pre}_{$module}_data_source", $id);
			$instance->custom_data    = get_field("{$pre}_{$module}_custom_data", $id);   // panel-4_fg_custom_data
			$instance->is_carousel    = get_field("{$pre}_{$module}_is_carousel", $id);
			$instance->feature_style  = get_field("{$pre}_{$module}_feature_style", $id);
			$instance->max_features   = get_field("{$pre}_{$module}_max_features", $id);
			$instance->column_layout  = get_field("{$pre}_{$module}_column_layout", $id);
			$instance->content_width  = get_field("{$pre}_{$module}_content_width", $id);
			// Get wp_query
			$item_max = $instance->max_features;
			$item_count = ($item_max > 0)?$item_max+1:$item_max;
			// Get Data frm Source
			if ($instance->data_source != "custom") {
				//
				// Data from Post type
				//
				$data = new WP_Query(
					array(
						'post_type' => $instance->data_source,
						'posts_per_page' => $item_count,
					)
				);
				// Get Features Objects
				$page_id = get_the_id();
				$display_count = 0;
				while ( $data->have_posts() ) {
					$data->the_post();
					// if ($_GET['debug']) {
						if ($item_max == -1 || $page_id != get_the_id()) {
							if ($item_max == -1 || $item_max > $display_count) {
								$feature = Feature::FromPostWithPre($instance->data_source);
								$instance->features[] = $feature;
								$display_count++;
							}
						}

					// } else {
					// 	$feature = Feature::FromPostWithPre($instance->data_source);
					// 	$instance->features[] = $feature;
					// 	$display_count++;
					// }
				}
			} else {
				//
				// Data from Repeater
				//
				$display_count = 0;
				foreach ($instance->custom_data as $new_feature) {
					if ($item_max == -1 || $item_max > $display_count) {
						$feature = Feature::FromRepeaterWithPre("{$pre}_{$module}_data",$new_feature);
						$instance->features[] = $feature;
						$display_count++;
					}
				}
				
			}
			return $instance;
		}

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="fg") {
			return FeatureGroup::FromPageIdWithPre($pre, "option", $module);
		}

	}
?>