<?php
	class CallToAction extends BOBject {
		public $button_text;
		public $button_link;
		public $button_style;
		public $button_modal;

		public function __construct() {
			parent::__construct();
		  $this->button_text  = null;
		  $this->button_link  = null;
		  $this->button_style = null;
		  $this->button_modal = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$module                  = "cta";
			$instance                = parent::FromPageWithPre($pre, $module);
		  $instance->button_text   = get_field("{$pre}_{$module}_button_text");
		  $instance->button_link   = get_field("{$pre}_{$module}_button_link");
		  $instance->button_style  = get_field("{$pre}_{$module}_button_style");
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$module                  = "cta";
			$instance                = parent::FromPageIdWithPre($pre, $id, $module);
		  $instance->button_text   = get_field("{$pre}_{$module}_button_text", $id);
		  $instance->button_link   = get_field("{$pre}_{$module}_button_link", $id);
		  $instance->button_style  = get_field("{$pre}_{$module}_button_style", $id);
			return $instance;
		}

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="cta") {
			return CallToAction::FromPageIdWithPre($pre, "option", $module);
		}

	}
?>