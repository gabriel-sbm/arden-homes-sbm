<?php
	class ImagePanel extends BOBject {

		public $full_height;
		public $parallax;

		public function __construct() {
			parent::__construct();
			$this->parallax = false;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$module                 = "ip";
			$instance               = parent::FromPageWithPre($pre, $module);
			$instance->image        = SBMImage::ValidateImage(get_field("{$pre}_{$module}_image"),['w'=>1440,'h'=>450]);
			$instance->full_height  = get_field("{$pre}_{$module}_full_height");
			$instance->parallax     = get_field("{$pre}_{$module}_parallax");
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$module                 = "ip";
			$instance               = parent::FromPageIdWithPre($pre, $id, $module);
			$instance->image        = SBMImage::ValidateImage(get_field("{$pre}_{$module}_image",$id),['w'=>1440,'h'=>450]);
			$instance->full_height  = get_field("{$pre}_{$module}_full_height",$id);
			$instance->parallax     = get_field("{$pre}_{$module}_parallax",$id);
			return $instance;
		}

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="ip") {
			return ImagePanel::FromPageIdWithPre($pre, "option", $module);
		}

	}
?>