<?php
	class Feature extends BOBject {
		public $excerpt;
		public $button_text;
		public $button_link;
		public $button_style;

		public function __construct() {
			parent::__construct();
			$this->excerpt            = null;
		  $this->button_text        = null;
		  $this->button_link        = null;
		  $this->button_style       = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$module                    = "f";
			$instance                  = parent::FromPageWithPre($pre, $module);
			$instance->excerpt         = BOBject::LimitString($instance->content, 12);
		  $instance->button_text     = get_field("{$pre}_{$module}_button_text");
		  $instance->button_link     = get_field("{$pre}_{$module}_button_link");
		  $instance->button_style    = get_field("{$pre}_{$module}_button_style");
		  if (is_null($instance->button_style) || empty($instance->button_style)) {
				$instance->button_style  = "btn btn-primary";
			}
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$module                    = "f";
			$instance                  = parent::FromPageIdWithPre($pre, $id, $module);
			$instance->excerpt         = BOBject::LimitString($instance->content, 12);
		  $instance->button_text     = get_field("{$pre}_{$module}_button_text", $id);
		  $instance->button_link     = get_field("{$pre}_{$module}_button_link", $id);
		  $instance->button_style    = get_field("{$pre}_{$module}_button_style", $id);
		  if (is_null($instance->button_style) || empty($instance->button_style)) {
				$instance->button_style  = "btn btn-primary";
			}
			return $instance;
		}

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="f") {
			return Feature::FromPageIdWithPre($pre, "option", $module);
		}

		public static function FromPostWithPre($pre) {
			if ($pre == "post") $pre   = "news";
			$instance                  = parent::FromPost();
			$instance->content         = get_field("{$pre}_feature_excerpt");
			$instance->excerpt         = BOBject::LimitString($instance->content, 12);
			$instance->button_link     = get_field("{$pre}_button_link");
			$instance->button_text     = get_field("{$pre}_button_text");
			$instance->button_style    = get_field("{$pre}_button_style");
		  if (is_null($instance->button_style) || empty($instance->button_style)) {
				$instance->button_style  = "btn btn-primary";
			}
			return $instance;
		}

		public static function FromRepeaterObjectWithPre($pre, $obj, $img_size=['w'=>300,'h'=>300]) {
			$instance                  = parent::FromRepeaterObjectWithPre($pre, $obj, $img_size);  // Title, Content & Image
		  $instance->button_text     = $obj["{$pre}_button_text"];
		  $instance->button_link     = $obj["{$pre}_button_link"];
		  if (is_null($instance->button_style) || empty($instance->button_style)) {
				$instance->button_style  = "btn btn-primary";
			}
			return $instance;
		}


	}
?>