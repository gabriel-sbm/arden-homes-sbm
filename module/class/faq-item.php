<?php
	class FAQItem {
		public $question;
		public $answer;

		public function __construct() {
		  $this->question          = null;
		  $this->answer            = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromObjectWithPre($obj, $pre="module") {
			$instance                = new self();
			$instance->question      = $obj["{$pre}_faq_question"];
			$instance->answer        = $obj["{$pre}_faq_answer"];
			return $instance;
		}

	}
?>