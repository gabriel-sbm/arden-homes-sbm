<?php
	class ContactEqualSplit extends BOBject {

		public $offices;

		public function __construct() {
			parent::__construct();
			$this->offices        = [];
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$module               = "ces";
			$instance             = parent::FromPageWithPre($pre, $module);
			$instance->offices    = Office::GetFieldsFromOptions();
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$module               = "ces";
			$instance             = parent::FromPageIdWithPre($pre, $id, $module);
			return $instance;
		}

		// FROM OPTIONS
		public static function FromOptions($pre="module", $module="ces") {
			return EqualSplit::FromPageIdWithPre($pre, "option", $module);
		}

	}
?>