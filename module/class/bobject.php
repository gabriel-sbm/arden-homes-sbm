<?php
	class BOBject {
		public $id;
		public $title_tag;
		public $title;
		public $content;
		public $image;
		public $width;
	  public $position;
		public $font;
		public $background;
		public $colour;
		public $style;
		public $permalink;

		public function __construct() {
			$this->id             = null;
			$this->title_tag      = "module_title";
			$this->title          = null;
			$this->content        = null;
		  $this->image          = null;
		  $this->width          = null;
		  $this->position       = null;
		  $this->font           = null;
		  $this->background     = null;
		  $this->colour         = null;
		  $this->style          = null;
		  $this->permalink      = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module", $module="none") {
			$font_colour          = get_field("{$pre}_{$module}_font");
			$instance             = new self();
			$instance->id         = $pre;
			$instance->title_tag  = get_field("{$pre}_{$module}_title_tag");
			$instance->title      = $instance->WrapTitle(get_field("{$pre}_{$module}_title"), $instance->title_tag, $font_colour);
			$instance->content    = get_field("{$pre}_{$module}_content");
		  $instance->image      = SBMImage::ValidateImage(get_field("{$pre}_{$module}_image"),['w'=>720,'h'=>390]);
		  $instance->width      = $instance->CleanWidth(get_field("{$pre}_{$module}_module_width"));
		  $instance->position   = get_field("{$pre}_{$module}_position");
		  $instance->background = get_field("{$pre}_{$module}_background");
		  $instance->font       = $font_colour;
		  $instance->colour     = $instance->CheckBackground($instance->background, get_field("{$pre}_{$module}_colour"));
		  if ($instance->background == "solid") {
		  	$instance->style    = "background-color:{$instance->colour};";
		  }
			return $instance;
		}

		// FROM PAGE ID WITH PRE
		//
		public static function FromPageIdWithPre($pre="module", $id="1", $module="none") {
			$font_colour          = get_field("{$pre}_{$module}_font", $id);
			$instance             = new self();
			$instance->id         = $pre;
			$instance->title_tag  = get_field("{$pre}_{$module}_title_tag", $id);
			$instance->title      = $instance->WrapTitle(get_field("{$pre}_{$module}_title", $id), $instance->title_tag, $font_colour);
			$instance->content    = get_field("{$pre}_{$module}_content", $id);
		  $instance->image      = SBMImage::ValidateImage(get_field("{$pre}_{$module}_image", $id),['w'=>720,'h'=>390]);
		  $instance->width      = $instance->CleanWidth(get_field("{$pre}_{$module}_module_width", $id));
		  $instance->position   = get_field("{$pre}_{$module}_position", $id);
		  $instance->background = get_field("{$pre}_{$module}_background", $id);
		  $instance->font       = $font_colour;
		  $instance->colour     = $instance->CheckBackground($instance->background, get_field("{$pre}_{$module}_colour", $id));
		  if ($instance->background == "solid") {
		  	$instance->style    = "background-color:{$instance->colour};";
		  }
			return $instance;
		}

		public static function FromOptions($pre="module", $module="none") {
			return BOBject::FromPageIdWithPre($pre, "option", $module);
		}

		public static function FromPost() {
			$instance             = new self();
			$instance->id         = get_the_id();
			$instance->title      = $instance->WrapTitle(get_the_title(), $instance->title_tag, $font_colour);
			$instance->content    = get_the_content();
		  $instance->image      = SBMImage::ValidateImage(get_the_post_thumbnail_url(),['w'=>720,'h'=>390]);
		  $instance->permalink  = get_permalink();
			return $instance;
		}

		public static function FromPostWithPre($pre) {
			return BOBject::FromPost();
		}	

		public static function FromRepeaterObjectWithPre($pre, $obj, $img_size=['w'=>720,'h'=>390]) {
			$instance             = new self();
			$instance->title      = $instance->WrapTitle($obj["{$pre}_title"], $instance->title_tag, $font_colour);
			$instance->content    = $obj["{$pre}_content"];
		  $instance->image      = SBMImage::ValidateImage($obj["{$pre}_image"],$img_size);
			return $instance;
		}	

		public static function LimitString($str, $count) {
			$split_str = explode(" ", $str);
			if (count($split_str) > $count) {
				$split_str = array_chunk($split_str, $count-1)[0];
				$split_str[count($split_str)-1] .= "...</p>";
			}
			return implode(' ', $split_str);
		}

		private function WrapTitle($str, $tag, $colour="#FFF") {
			$style = "";
			if (!is_null($colour) && !empty($colour)) {
				$style = " style=\"color:{$colour}\"";
			}
			if (!empty($str)) {
				if ($tag == "module_title") {
					$str = "<div class=\"$tag\"{$style}><div>{$str}</div></div>";
				} else {
					$str = "<{$tag}{$style}>{$str}</{$tag}>";
				}
			}
			return $str;
		}

		private function CleanWidth($str) {
			if ($str == "default") {
				$str = "";
			}
			return $str;
		}

		private function CheckBackground($bg, $colour) {
			if ($bg != "solid") $colour = null;
			return $colour;
		}



	}
?>