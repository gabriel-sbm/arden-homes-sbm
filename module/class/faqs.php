<?php
	class FAQs {
		public $title_tag;
		public $title;
		public $faqs;

		public function __construct() {
			$this->title_tag         = "module_title";
		  $this->title             = null;
		  $this->faqs              = null;
		}

		// FROM PAGE WITH PRE
		//
		public static function FromPageWithPre($pre="module") {
			$instance                = new self();
			$instance->title_tag     = get_field("{$pre}_faq_title_tag");
			$instance->title         = $instance->WrapTitle(get_field("{$pre}_faq_title"), $instance->title_tag);
			$faq_repeater            = get_field("{$pre}_faq_repeater");
			if (!is_null($faq_repeater) && !empty($faq_repeater)) {
				foreach ($faq_repeater as $faq) {
					$instance->faqs[]      = FAQItem::FromObjectWithPre($faq, $pre);
				}
			}
			return $instance;
		}

		private function WrapTitle($str, $tag) {
			if (!empty($str)) {
				if ($tag == "module_title") {
					$str = "<div class=\"$tag\">{$str}</div>";
				} else {
					$str = "<{$tag}>{$str}</{$tag}>";
				}
			}
			return $str;
		}

	}
?>