<?php
	/* Explore the Possibilities
	*/
	$home_id = get_option('page_on_front');
	$panel_4     =  [
		'title'    => get_field('panel_4_title',$home_id),
		'content'  => get_field('panel_4_content',$home_id),
		'button_1' => get_field('panel_4_button_1_text',$home_id),
		'url_1'    => get_field('panel_4_button_1_link',$home_id),
		'button_2' => get_field('panel_4_button_2_text',$home_id),
		'url_2'    => get_field('panel_4_button_2_link',$home_id),
	];
	$panel_4['title'] = $bottom_panel['title'];
	$panel_4['content'] = $bottom_panel['content'];
?>
	<div class="content panel_4">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<h2><?=embolden($panel_4['title'],-1)?></h2>
					<?=$panel_4['content']?>
				</div>
				<div class="col-lg-4 button_group">
					<?php
						$btn_count = 0;
						if (!empty($panel_4['button_1']) && !empty($panel_4['url_1'])) {
							$btn_count++;
							echo "<a href=\"{$panel_4['url_1']}\" class=\"btn\">{$panel_4['button_1']}</a>";
						}
						if (!empty($panel_4['button_2']) && !empty($panel_4['url_2'])) {
							if ($btn_count > 0) {
								echo "<p class=\"or\">Or</p>";
							}
							echo "<a href=\"{$panel_4['url_2']}\" class=\"btn btn-secondary\">{$panel_4['button_2']}</a>";
						}
					?>
				</div>
			</div>
		</div>
	</div>