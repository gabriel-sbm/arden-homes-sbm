<?php
	/*
			CAROUSEL
		
	*/
	$testimonials = Carousel::TestimonialsWithPre('testimonials', 'slide-panel');
	$testimonials->indicator = 'circle';
	$testimonials->captions  = true;

	if (!is_null($testimonials->panel->colour) && !empty($testimonials->panel->colour)) {
		$testimonials->bgcolour = $testimonials->panel->colour;
	}

	$slides = get_field('home_testimonials');

	if (count($slides) > 0) {
		foreach ($slides as $slide) {
			$new_slide = Slide::FromOptionRowWithPre($slide, 'testimony_');
			$testimonials->slide[] = $new_slide;
		}
	}
 
	// // Build slider
	if (count($testimonials->slide) > 0) {
		$slide_count = 0;
		$indicators = "";
		$t10l_inner = "";

		foreach ($testimonials->slide as $slide) {
			$active = '';
			if ($slide_count == 0) {
				$active = 'active';
			}
			$indicators .= "\t\t\t<li data-target=\"#testimonial_carousel\" data-slide-to=\"{$slide_count}\" class=\"{$active}\"></li>\r\n";
			$t10l_inner .= "
				<div class=\"carousel-item {$active}\">
			";
			if ($testimonials->captions) {
				$t10l_inner .= "
					<div class=\"testimonial-caption\">
				";
				if (!is_null($slide->content) && !empty($slide->content)) {
					$t10l_inner .= "
				    <div class=\"slide-testimony\">
				    	{$slide->content}
				    </div>
					";
				}
				if (!empty($slide->title) || !empty($slide->link)) {
					$t10l_inner .= "<div class=\"slide-author\">";
					if (!empty($slide->title)) {
						$t10l_inner .= "{$slide->title}";
						if (!empty($slide->link)) {
							$t10l_inner .= ", {$slide->link}";
						}
					} else
					if (!empty($slide->link)) {
						$t10l_inner .= "{$slide->link}";
					}
				}
				$t10l_inner .= "
				  </div>
				";
			}
			$t10l_inner .= "
				</div>
			";
			$slide_count++;
		}
?>

<div class="container">
	<div class="row">
		<div id="testimonial_carousel" class="col-12 carousel <?=$testimonials->animation?>" data-ride="carousel" data-interval="0" style="background-color:<?=$testimonials->bgcolour?>">
			<?=$testimonials->panel->title?>
			<?php
				// Indicators..
				if (count($testimonials->slide) > 1) {
					echo "
				<ul class=\"carousel-indicators indicator-{$testimonials->indicator}\">
					{$indicators}
				</ul>
					";
				}
				// Slides
			?>
			<div class="carousel-inner">
				<?=$t10l_inner?>
			</div>
			<?php
				// Left / Right Arrows
				if (count($testimonials->slide) > 1 && $testimonials->navigation) {
			?>
			<a class="carousel-control-prev p4-caret-left-light" href="#testimonial_carousel" data-slide="prev"></a>
			<a class="carousel-control-next p4-caret-right-light" href="#testimonial_carousel" data-slide="next"></a>
			<?php
					}
			echo "
		</div>
	</div>
</div>
		";
		}
	?>


