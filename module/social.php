<?php
/* SOCIAL
*/
	$socials = get_field('socials','options');
	if (count($socials) > 0) {
		echo "<div class=\"social\">";
		foreach ($socials as $social) {
			echo "<a href=\"{$social['url']}\" target=\"_blank\" class=\"{$social['slug']}\"></a>";
		}
		echo "</div>";
	}
?>