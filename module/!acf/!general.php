<?php


				// Tab
				//
				array(
					'key' => 'theme-options-locations-tab',
					'label' => 'Office Details',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),


				// Message
				//
				array(
					'key' => 'field_5c26ca0e3b400',
					'label' => 'Message',
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => 'Example message!',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),




				// Input field
				//
				array(
					'key' => 'page-heading',
					'label' => 'Page Heading',
					'name' => 'page_heading',
					'type' => 'text',
					'instructions' => 'This is the main heading for the page (H1).',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),


				// Wysiwyg
				//
				array(
					'key' => 'page-introduction',
					'label' => 'Introduction',
					'name' => 'page_introduction',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),


				// Image
				//
				array(
					'key' => 'home-feature-image',
					'label' => 'Feature Image',
					'name' => 'home_feature_image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),


				// Repeater
				//
				array(
					'key' => 'promotions-repeater',
					'label' => 'Promotions',
					'name' => 'promotions',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => '',
					'sub_fields' => array(
						// Fields
						array(
							'key' => 'feature-heading',
							'label' => 'Heading',
							'name' => 'feature_heading',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),


					),
				),


				// Checkbox
				//
				array(
					'key' => 'field_5c245fe955142',
					'label' => 'Use Slide Group',
					'name' => 'use_slide_group',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),


				// Show
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5c245fe955142',
							'operator' => '==',
							'value' => '1',
						),
					),
				),

				// Hide
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5c245fe955142',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),



				// File
				//
				array(
					'key' => 'field_5c26cb969b98b',
					'label' => 'Download PDF',
					'name' => 'download_pdf',
					'type' => 'file',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'url',
					'library' => 'all',
					'min_size' => '',
					'max_size' => '',
					'mime_types' => '',
				),




