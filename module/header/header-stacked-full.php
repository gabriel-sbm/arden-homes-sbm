	<header class="fixed-top stacked full-width">
		<div class="primary-nav">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<div class="row">
						<div class="col-2 d-lg-none">
							<div id="navbar-toggle" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="true" aria-label="Toggle navigation" class="closed">
								<span></span><span></span><span></span>
							</div>
						</div>
						<div class="col-3 d-none d-lg-block phone">
							<a href="tel:<?=get_field('header_phone', 'options')?>">Call <?=get_field('header_phone', 'options')?></a>
						</div>
						<div class="col-8 col-lg-6 brand">
							<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
								<img class="svg" src="<?php bloginfo('template_url'); ?>/assets/images/arden-logo-black.svg" alt="">
							</a>
						</div>
						<div class="col-2 col-lg-3 search">
							<a href="#" class="p4-search-1"></a>
						</div>
						<div class="d-none d-lg-block col-12 by-line">
							<p><?=get_field('header_by_line', 'options')?></p>
						</div>
						<div class="col-12">
							<div class="collapse navbar-collapse" id="primaryNav">
							<?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '<ul class="navbar-nav mr-auto">%3$s</ul>', 'container' => false, 'fallback_cb' => false) ); ?> 
							</div>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</header>