	<header class="fixed-top">
		<div class="primary-nav">
			<div class="container">
				<nav class="navbar navbar-expand-lg navbar-light">
					<div class="row">
						<div class="col-12">
							<div id="navbar-toggle" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="true" aria-label="Toggle navigation" class="closed">
								<span></span><span></span><span></span>
							</div>
							<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
								<img src="http://ph4se.com.au/app/image/?text=Organisation Logo&w=300&h=80&size=20" width="300" height="80" class="img-fluid">
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="collapse navbar-collapse" id="primaryNav">
							<?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '<ul class="navbar-nav mr-auto">%3$s</ul>', 'container' => false, 'fallback_cb' => false) ); ?> 
							</div>
						</div>
					</div>
					<div class="row social-row">
						<div class="col-12">
							<?php
								include_once __DIR__ . '/../social.php';
							?>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</header>