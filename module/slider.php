<?php
$query = new WP_Query(array(
    'post_type' => 'slider'
));
if( $query->have_posts() ){
?>

  <div id="carousel-slider" class="carousel slide" data-ride="carousel" data-interval="9000" data-pause="hover" >

    <!-- Indicators -->
    <?php
      $indicators = '<ol class="carousel-indicators">';
      $loop = new WP_Query( array( 'post_type' => 'slider') );
      $a=0;
      while ( $loop->have_posts() ) : $loop->the_post();
        $indicators .= '<li data-target="#carousel-slider" data-slide-to="'.$a.'" class="'.(($a==0)?'active':'').'"></li>';
        $a++;
      endwhile;
      wp_reset_query();
      $indicators .= '</ol>';
      if ($a > 1) {
        echo $indicators;
      }
    ?>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <?php $loop = new WP_Query( array( 'post_type' => 'slider') ); ?>
        <?php $b=0; while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <div class="item <?php if($b==0){ echo 'active';}else{ echo ''; } ?>">

            <?php if (has_post_thumbnail( $post->ID ) ): ?>
              <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
              <div class="custom-bg" style="background-image: url('<?php echo $image[0]; ?>')">
                <div class="container">
                <div class="carousel-caption">
                  <h1><?php the_title(); ?></h1>
                  <p><?php the_content(); ?></p>
                </div>
              </div>
              </div>
            <?php endif; ?>

          </div>
      <?php $b++; endwhile; wp_reset_query(); ?>
    </div>

    <!-- Controls -->
    <?php if ($a > 1) { ?>
    <a class="left carousel-control" href="#carousel-slider" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-slider" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    <?php } ?>

  </div>

<?php } else { ?>
  <div class="text-center">No Slider, please add your first slider.</div>
<?php }
?>
