<?php
	if (count($offices) > 0) {
		foreach ($offices as $office) {
			if ($office->primary) {
?>
	<div class="google-map">
  	<div class="acf-map">
			<div class="marker" data-lat="<?=$office->address['lat']?>" data-lng="<?=$office->address['lng']?>"></div>
		</div>
	</div>
<?php
			}
		}
	} 
?>