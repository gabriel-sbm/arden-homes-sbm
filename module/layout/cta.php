	<section class="<?=$module->id?> cta <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container block-bg">
			<div class="row">
				<div class="<?=($module->position=='es-centre')?'col-12 ':'col-md-6 '?>cta-block">
					<div>
						<?=$module->title?>
						<?php
							if (!empty($module->content)) {
								echo $module->content;
							}
						?>
					</div>
				</div>
				<div class="<?=($module->position=='es-centre')?'col-12 ':'col-md-6 '?>cta-button">
					<div>
						<?php
							if (!empty($module->button_text)) {
								if (!is_null($module->button_modal) && !empty($module->button_modal)) {
									echo "<button type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" data-target=\"#{$module->button_modal}\">{$module->button_text}</button>";
								} else {
									echo "<a href=\"{$module->button_link}\" class=\"{$module->button_style}\">{$module->button_text}</a>";
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
