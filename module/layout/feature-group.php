	<section class="<?=$module->id?> feature-group <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">
				<?php
					if (!is_null($module->title) && !empty($module->title)) {
				?>
				<div class="col-12"><?=$module->title?></div>
				<?php
					}
					$feature_item = 0;
					if (!is_null($module->features) && !empty($module->features)) {
						if (!is_null($module->feature_style) && !empty($module->feature_style)) {
							echo "
								<div class=\"col-12 feature-container\">
									<ul class=\"row\">
							";
							foreach ($module->features as $feature) {
								$feature_style = $module->feature_style;
								$cols = " col-sm-4";
								// Check for Featured image
								if ($feature_style == "title-background-featured") {
									$feature_style = "title-background";
									if ($feature_item == 0) {
										$cols = " col-sm-12 featured_image";
									}
								}
								if (is_null($feature->button_link) || empty($feature->button_link)) {
									// No Button Text - No Link
									$feature_style .= "-no-link"; 
									$feature->button_link = $feature->permalink;
								} else if ($feature->button_link == "#") {
									// Get Item Link
									$feature->button_link = $feature->permalink;
								}
								$feature_card = include("features/{$feature_style}.php");
								$feature_card = str_replace("##style##", $cols, $feature_card);
								$feature_card = str_replace("##title##", $feature->title, $feature_card);
								$feature_card = str_replace("##content##", $feature->content, $feature_card);
								$feature_card = str_replace("##excerpt##", $feature->excerpt, $feature_card);
								$feature_card = str_replace("##background-image##", $feature->image->url, $feature_card);
								$feature_card = str_replace("##button_link##", $feature->button_link, $feature_card);

								$button_format = "";
								if (!is_null($feature->button_text) && !empty($feature->button_text)) {
									$button_format = "<button class=\"##button_style##\">##button_text##</button>";
									$button_format = str_replace("##button_text##", $feature->button_text, $button_format);
									$button_format = str_replace("##button_style##", $feature->button_style, $button_format);
								}
								$feature_card = str_replace("##button##", $button_format, $feature_card);

								echo $feature_card;
								$feature_item++;
							}
							echo "
									</ul>
								</div>
							";
						} else {
							echo "A feature style or data source has not been selected.";
						}
					}

				?>

			</div>
		</div>
	</section>
