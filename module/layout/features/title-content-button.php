<?php
	// Title, Content & Button
	//
	return "
		<li class=\"feature-card feature-tcb##style##\">
			<a href=\"##button_link##\">
				##title##
				<div class=\"feature-content\">
					##content##
					##button##
				</div>
			</a>
		</li>
	";
?>