	<section class="<?=$module->id?> ctae <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container block-bg">
			<div class="row">
				<div class="col-md-6 cta-block">
					<?=$module->title?>
					<?php
						if (!empty($module->content)) {
							echo $module->content;
						}
					?>
				</div>
				<div class="col-md-6 cta-button">
					<div>
						<?php
							if (!empty($module->button_text_open)) {
								echo "<button id=\"toggle_expand\" type=\"button\" class=\"{$module->button_style}\" data-open=\"{$module->button_text_open}\" data-closed=\"{$module->button_text_closed}\">{$module->button_text_closed}</button>";
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="calculator">
			<div class="container">
				<div class="row">
					<?php
						if (!empty($module->hidden_content)) {
							echo "
								<div class=\"col-12\">
									{$module->hidden_content}
								</div>
							";
						}
					?>
				</div>
			</div>
		</div>
	</section>
