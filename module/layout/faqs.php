	<section class="<?=$module->id?> faq-panel <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php
						if (!is_null($module->title) && !empty($module->title)) {
							echo $module->title;
						}

						if (count($module->faqs) > 0) {
							echo "<div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"false\">";
							$item = 0;
							foreach ($module->faqs as $faq) {
								echo "
								<div class=\"panel panel-default\">
									<div class=\"panel-heading\" role=\"tab\" id=\"heading_{$item}\">
										<a role=\"button\" class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse_{$item}\" aria-expanded=\"true\" aria-controls=\"collapse_{$item}\">
											<h4 class=\"panel-title\">
												{$faq->question}
												<span class=\"chev-acc pull-right\"><i class=\"fa fa-chevron-down\" style=\"display:inline-block;\"></i></span><span class=\"chev-acc pull-right\"><i class=\"fa fa-chevron-up\" style=\"display:none;\"></i></span>
											</h4>
										</a>
									</div>
									<div id=\"collapse_{$item}\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading_{$item}\">
										<div class=\"panel-body\">
											{$faq->answer}
										</div>
									</div>
								</div>
								";
								$item++;
							}
							echo "</div>";
						} else {
							echo "There are currently no Frequently Asked Questions.";
						}
					?>					
				</div>
			</div>
		</div>
	</section>


<?php /*


							<?php if( have_rows('faq') ): ?>
								<div class="panel-group" id="accordion<?php echo $a; ?>" role="tablist" aria-multiselectable="false">
								<?php $b=1; while ( have_rows('faq') ) : the_row(); ?>

								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading<?php echo $a; ?><?php echo $b; ?>">
										<a role="button" data-toggle="collapse" data-parent="#accordion<?php echo $a; ?>" href="#collapse<?php echo $a; ?><?php echo $b; ?>" aria-expanded="true" aria-controls="collapse<?php echo $a; ?><?php echo $b; ?>">
											<h4 class="panel-title">
													 <?php the_sub_field('question'); ?>

											 <span class="chev-acc pull-right"><i class="fa fa-chevron-down" style="display:inline-block;"></i></span><span class="chev-acc pull-right"><i class="fa fa-chevron-up" style="display:none;"></i></span>
										 </h4>
										 </a>
										</div>
										<div id="collapse<?php echo $a; ?><?php echo $b; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $a; ?><?php echo $b; ?>">
													<div class="panel-body">
														<?php the_sub_field('answer'); ?>
													</div>
										</div>
								</div>
								<?php	$b++; endwhile; ?>
							</div>
					<?php else : endif;?>
				<?php	$a++; endwhile; ?>

*/ ?>