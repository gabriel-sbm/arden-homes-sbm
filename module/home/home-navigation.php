		<div id="secondaryNav">
			<nav class="navbar">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<ul class="navbar-nav mr-auto">
								<li class="nav-item<?=($home_page=='overview')?' active':''?>"><a href="#overview">Overview</a></li>
								<li class="nav-item nav-item-has-children<?=($home_page=='facades'||$home_page=='interiors')?' active':''?>"><a href="#facades">Gallery</a>
									<?php 
										if (is_array($home->gallery_interior_slides) && count($home->gallery_interior_slides) > 0) {
									?>
									<ul class="sub-menu">
										<li class="nav-item<?=($home_page=='facades')?' active':''?>"><a href="#facades">Facades</a></li>
										<li class="nav-item<?=($home_page=='interiors')?' active':''?>"><a href="#interiors">Interiors</a></li>
									</ul>
									<?php
										}
									?>
								</li>
									<?php
										if (!is_null($home->floorplan->layouts) && !empty($home->floorplan->layouts) && count($home->floorplan->layouts) > 0) {

											$parent_link = strtolower(trim($home->title));
											if (!empty($home->floorplan->layouts[0]->name)) {
												$parent_link .= "-".strtolower(str_replace(" ", "-", $home->floorplan->layouts[0]->name));
											}
											echo "
												<li class=\"nav-item nav-item-has-children".(($home_page==$parent_link)?' active':'')."\"><a href=\"#{$parent_link}\">Floorplans</a>
											";
											if (count($home->floorplan->layouts) > 1) {
												echo "
														<ul class=\"sub-menu\">
												";
												foreach ($home->floorplan->layouts as $plan) {
													$page_link = strtolower(trim($home->title));
													if (!empty($plan->name)) {
														$plan_name = str_replace(" ", "-", $plan->name);
														$page_link .= "-{$plan_name}";
													}

													echo "<li class=\"nav-item".(($home_page==$page_link)?' active':'')."\"><a href=\"#{$page_link}\">".trim($home->title)." {$plan->name}</a></li>";
												}
												echo "
														</ul>
												";
											}
											echo "
												</li>
											";
										}
									?>
								<li class="nav-item<?=($home_page=='inclusions')?' active':''?>"><a href="#inclusions">Inclusions</a></li>
								<?php
									if ($home->promotion_show == 1) {
										echo "<li class=\"nav-item".(($home_page=='promotion')?' active':'')."\"><a href=\"#promotion\">Promotion</a></li>";
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		</div>