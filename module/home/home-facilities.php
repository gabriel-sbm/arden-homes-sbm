<?php
	if (!is_null($home->features) && !empty($home->features) && count($home->features) > 0) {
		echo "
		<section class=\"feature-panel\">
			<div class=\"container\">
				<div class=\"row\">
					<ul class=\"col-12 feature-items\">
		";
		foreach ($home->features as $feature) {
			echo "<li><span class=\"{$feature['type']}\"></span>{$feature['amount']}</li>";
		}
			echo "
					</ul>
				</div>
			</div>
		</section>
		";
	}
?>