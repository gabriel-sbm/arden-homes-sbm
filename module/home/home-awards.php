<?php
	echo "
		<div class=\"awards_list\">
			<div class=\"container\">
				<h2 class=\"sr-only\">AWARDS</h2>
				<ul class=\"row\">
	";
	$award_list = [];
	$image_url = 0;
	foreach ($home->awards as $award) {
		$award_data = $award['awards'][0];
		$award_image_id = get_post_thumbnail_id($award_data->ID);
		$award_image_url = wp_get_attachment_image_src($award_image_id, 'full');
		if (!is_array($award_list[$award_data->award_year])) {
			$award_list[$award_data->award_year] = [];
		}
		$award_list[$award_data->award_year][] = "
					<li class=\"col-12\">
						<div class=\"row\">
							<div class=\"award_logo\">
								<img src=\"{$award_image_url[$image_url]}\" alt=\"\">
							</div>
							<div class=\"award_copy\">
								<h2><strong>{$award_data->award_year}:</strong> {$award['award_title']}</h2>
								<strong>{$award_data->post_title}</strong><br>
								".(!empty($award['award_description'])?$award['award_description']."<br>":"")."
							</div>
						</div>
					</li>
		";
	}
	// Output Awards
	krsort($award_list);
	foreach ($award_list as $awards) {
		foreach ($awards as $award) {
			echo $award;
		}
	}
	echo "
				</ul>
			</div>
		</div>
	";

?>
