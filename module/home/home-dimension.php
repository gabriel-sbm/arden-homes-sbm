<?php
				$highlight_row = [ "room dimensions",  "house dimensions", "total area" ];
				$plan_name = str_replace(" ", "-", $layout_data->name);
				$page_link = "#".strtolower(trim($home->title))."-{$plan_name}";
				echo "
							<div class=\"col-12 col-lg-4{$wrap_class}\">
								<div class=\"dimension-block\">
									<a class=\"floorplan_link\" href=\"{$page_link}\"><h3>".trim($home->title)."<span>{$layout_data->name}</span></h3></a>
									<ul>
				";

				foreach ($layout_data->dimensions as $dimension) {
					$row_data = [
						'label' => $dimension['label'],
						'size'  => $dimension['size']
					];
					if (in_array(strtolower($dimension['label']), $highlight_row)) {
						$row_data['label'] = "<strong>{$row_data['label']}</strong>";
						$row_data['size']  = "<strong>{$row_data['size']}</strong>";
					}
					echo "	<li><div>{$row_data['label']}</div><div></div><div>{$row_data['size']}</div></li>";
				}
				echo "
									</ul>
									<a href=\"{$layout_data->download}\" target=\"_blank\" class=\"btn btn-range\">Download Floorplan {$layout_data->name}</a>
				";
				if (count($layout_data->options) > 0) {
					echo "
									<button type=\"button\" class=\"btn btn-range\" data-toggle=\"modal\" data-target=\"#modal_".trim($home->title)."_{$layout_data->name}\">View Floorplan Options</button>
					";
				}
				echo "
								</div>
							</div>
				";

?>