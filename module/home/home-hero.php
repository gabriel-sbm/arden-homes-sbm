		<div class="hero-panel">
			<div class="container">
				<div class="row">
					<div class="col-12 hero-image" style="background-image:url(<?=$module['image']?>);">
						<?php
							if (!is_null($module['caption']) && !empty($module['caption'])) {
						?>
						<div class="animated fadeIn hero-caption">
							<div class="slide-title"><?=$module['caption']?></div>
						</div>
						<?php 
							}
						?>
					</div>
				</div>
			</div>
		</div>
