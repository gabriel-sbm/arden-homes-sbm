<?php
// CAROUSEL - DIRTY
//
	// Get Defaults
	$carousel = Carousel::FromOptions();
	if (!is_null($slides) && !empty($slides)) {
?>
<div id="carouselPanel" class="carousel slide control-external" data-ride="carousel">
  <?php
		// Indicators
		if (count($slides) > 1) {
			$indicators = "";
	 		for ($int=0; $int<count($slides); $int++) {
	 			$active = ($int == 0)?" active":"";
	 			$indicators .= "<li data-target=\"#carouselPanel\" data-slide-to=\"{$int}\" class=\"{$active}\"></li>";
	 		}

			echo "
		<ul class=\"carousel-indicators indicator-{$carousel->indicator}\">
			{$indicators}
		</ul>
			";
		}
 	?>
  <div class="carousel-inner" role="listbox">
  	<?php
  		$int = 0;
  		foreach ($slides as $image) {
  			$active = ($int++ == 0)?" active":"";
	  		echo "
			    <div class=\"carousel-item{$active}\">
			      <img class=\"d-block img-fluid\" src=\"{$image['url']}\" alt=\"{$image['alt']}\">
			    </div>
			  ";
  		}

  	?>
  </div>
  <?php
  	// Arrows
 		if (count($slides) > 1) {
	?>
  <a class="carousel-control-prev p4-caret-left" href="#carouselPanel" role="button" data-slide="prev">
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next p4-caret-right" href="#carouselPanel" role="button" data-slide="next">
    <span class="sr-only">Next</span>
  </a>
  <?php
  	}
  ?>
</div>

<script>
	// CAROUSEL
	$(function() {
		if ($('#carouselPanel')) {
			$('#carouselPanel').carousel({
				interval: <?=$carousel->delay?>
			});
		}
	});
</script>
<?php
	}
?>
