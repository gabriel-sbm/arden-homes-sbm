		<div id="<?=$module_details['id']?>" class="content module-block sbm-image-carousel">
			<div class="container">
				<div class="contentwrap">
<!-- 					<div class="row">
						<div class="col-sm-12">
 -->							<div class="main">
								<?php
									if (!is_null($module_details['title']) && !empty($module_details['title'])) {
										echo "<h2>".embolden($module_details['title'])."</h2>";
									}
								?>
								<div class="image-carousel-wrap">
									<ul>
									<?php
										foreach ($data_source as $product) {
											echo "
												<li>
													<a href=\"{$product->permalink}\">
														<h3>".embolden($product->title)."</h3>
														<div style=\"background-image:url({$product->feature['url']});\"></div>
														<button class=\"btn btn-secondary\">Read More</button>
													</a>
												</li>
											";
										}
									?>
									</ul>
								</div>
<!-- 							</div>
						</div>
 -->					</div>
				</div>
			</div>
		</div>
