<?php
/* CAROUSEL - DIRTY
*/
	$carousel = Carousel::FromOptions();
	$slides = get_field('carousel');
	if (!is_null($slides) && !empty($slides)) {
?>
<div id="carouselDirty" class="carousel slide" data-ride="carousel">
  <?php
		// Indicators
		if (count($slides) > 1) {
			$indicators = "";
	 		for ($int=0; $int<count($slides); $int++) {
	 			$active = ($int == 0)?" active":"";
	 			$indicators .= "<li data-target=\"#carouselDirty\" data-slide-to=\"{$int}\" class=\"{$active}\"></li>";
	 		}

			echo "
		<ul class=\"carousel-indicators indicator-{$carousel->indicator}\">
			{$indicators}
		</ul>
			";
		}
 	?>
  <div class="carousel-inner" role="listbox">
  	<?php
  		$int = 0;
  		foreach ($slides as $image) {
  			$active = ($int++ == 0)?" active":"";
	  		echo "
			    <div class=\"carousel-item{$active}\">
			      <img class=\"d-block img-fluid\" src=\"{$image['image']['url']}\" alt=\"{$image['image']['alt']}\">
			    </div>
			  ";
  		}

  	?>
  </div>
  <a class="carousel-control-prev" href="#carouselDirty" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselDirty" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<script>
	// CAROUSEL
	$(function() {
		if ($('#carouselDirty')) {
			$('#carouselDirty').carousel({
				interval: <?=$carousel->delay?>
			});
		}
	});
</script>
<?php
	}
?>
