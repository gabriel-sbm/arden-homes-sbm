<?php
	/*
			CAROUSEL
		
	*/

	// If Slider is empty but 'use_placeholders' is not, then use this.
	if ((is_null($carousel->slides) || empty($carousel->slides[0])) && $carousel->use_placeholders) {
		$slide_count = 0;
		$create_slides = 3;
		$images_provided = false;
		// If Placeholders is not empty, use the images
		if (count($carousel->placeholders) > 0) {
			$images_provided = true;
			$create_slides = count($carousel->placeholders);
		}
		// Create slides
		for ($i=0; $i<$create_slides; $i++) { 
			$new_slide = new Slide();
			$new_slide->title = "Carousel slide ".(++$slide_count);
			// $new_slide->content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>";
			if ($images_provided) {
				$new_slide->image = $carousel->placeholders[$i];
			}
			$carousel->slides[] = $new_slide;
		}
	}

	// Build slider
	if (count($carousel->slides) > 0) {
		$slide_count = 0;
		$indicators = "";
		$inner = "";
		$preload = "";          // Background images don't pre-load, these will.

		foreach ($carousel->slides as $slide) {
			$active = '';
			if ($slide_count == 0) {
				$active = 'active';
			}
			$preload .= "<img src=\"{$slide->image->url}\" alt=\"\">";
			$indicators .= "\t\t\t<li data-target=\"#carousel_home\" data-slide-to=\"{$slide_count}\" class=\"{$active}\"></li>\r\n";
			$inner .= "
				<img src=\"{$slide->image->url}\" alt=\"\" class=\"carousel-item {$active}\">
			";
			$slide_count++;
		}
?>
<h2 class="sr-only">Carousel</h2>
<div class="container">
	<div class="row">
		<div id="<?=$carousel->id?>" class="col-12 carousel <?=$carousel->animation?>" data-ride="carousel" data-interval="<?=$carousel->delay?>" style="background-color:<?=$carousel->bgcolour?>">
			<div class="pre-load"><?=$preload?></div>
			<?php
				// Indicators..
				if (count($carousel->slides) > 1) {
					echo "
				<ul class=\"carousel-indicators indicator-{$carousel->indicator}\">
					{$indicators}
				</ul>
					";
				}
				// Slides
				$height = (!is_null($carousel->height))?" style=\"height:{$carousel->height}px;\"":"";
			?>
			<div class="carousel-inner" data-height="<?=$carousel->height?>"<?=$height?>>
				<?=$inner?>
			</div>
			<?php
				// Left / Right Arrows
				if (count($carousel->slides) > 1 && $carousel->navigation) {
			?>
			<a class="carousel-control-prev p4-caret-left-light" href="#<?=$carousel->id?>" data-slide="prev"></a>
			<a class="carousel-control-next p4-caret-right-light" href="#<?=$carousel->id?>" data-slide="next"></a>
	<?php
		}
		echo "
			</div>
		</div>
	</div>
	";
	}
	?>

<script>
	// CAROUSEL
	var carousel_full_screen = false;
</script>
