<?php
	/*
			CAROUSEL v1.1
			-- Added link if valid
		
	*/

	// If Slider is empty but 'use_placeholders' is not, then use this.
	if ((is_null($carousel->slides) || empty($carousel->slides[0])) && $carousel->use_placeholders) {
		$slide_count = 0;
		$create_slides = 3;
		$images_provided = false;
		// If Placeholders is not empty, use the images
		if (is_array($carousel->placeholders) && count($carousel->placeholders) > 0) {
			$images_provided = true;
			$create_slides = count($carousel->placeholders);
		}
		// Create slides
		for ($i=0; $i<$create_slides; $i++) { 
			$new_slide = new Slide();
			$new_slide->title = "Carousel slide ".(++$slide_count);
			// $new_slide->content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>";
			if ($images_provided) {
				$new_slide->image = $carousel->placeholders[$i];
			}
			$carousel->slides[] = $new_slide;
		}
	}

	// Build slider
	if (is_array($carousel->slides) && count($carousel->slides) > 0) {
		$slide_count = 0;
		$indicators = "";
		$inner = "";
		$preload = "";          // Background images don't pre-load, these will.

		foreach ($carousel->slides as $slide) {
			$active = '';
			if ($slide_count == 0) {
				$active = 'active';
			}
			$preload .= "<img src=\"{$slide->image->url}\" alt=\"\">";
			$indicators .= "\t\t\t<li data-target=\"#carousel_home\" data-slide-to=\"{$slide_count}\" class=\"{$active}\"></li>\r\n";
			$inner .= "
				<div class=\"carousel-item {$active}\">
			";
			if (!is_null($slide->link) && !empty($slide->link)) {
				$inner .= "
					<a href=\"{$slide->link}\"".((!$slide->interal)?" target=\"_blank\"":"").">
				";
			}
			$inner .= "
					<div class=\"img\" style=\"height:{$carousel->height}px; background-image:url('{$slide->image->url}');\"></div>
			";
			if ($carousel->captions && (!empty($slide->title) || !empty($slide->content))) {
				if ($slide->feature) {
					//
					// Feature style!!
					//
				 	$caption_type = [
				 		'start' => "div",
				 		'end'   => "div"
				 	];
				 	if (!empty($slide->link)) {
					 	$caption_type['start'] = "a href=\"{$slide->link}\"".((!$slide->internal)?" target=\"_blank\"":"");
					 	$caption_type['end'] = "a";
				 	}

					if (is_page(5)) {
							$inner .= "
								<{$caption_type['start']} class=\"animated fadeIn carousel-caption caption-feature carousel-home\" style=\"animation-delay:0.5s\">
									<div class=\"caption-container\">
									<div class=\"slide-title\">".$slide->title."</div>
							";
					} else {
							$inner .= "
								<{$caption_type['start']} class=\"animated fadeIn carousel-caption caption-feature\" style=\"animation-delay:0.5s\">
									<div class=\"caption-container\">
									<div class=\"slide-title\">".$slide->title."</div>
							";
					}
					if (!is_null($slide->content) && !empty($slide->content)) {
						$inner .= "
					    <div class=\"slide-content\">
					    	{$slide->content}
					    </div>
						";
					}
					$inner .= "
							</div>
					  </{$caption_type['end']}>
					";
				} else {
					if (is_page(5)) {
						$inner .= "
						<div class=\"animated fadeIn carousel-caption carousel-home\" style=\"animation-delay:0.5s\">
						<div class=\"caption-container\">
					    <div class=\"slide-title\">".$slide->title."</div>";
					} else {
						$inner .= "
						<div class=\"animated fadeIn carousel-caption\" style=\"animation-delay:0.5s\">
					    <div class=\"slide-title\">".$slide->title."</div>";
					}
					
					if (!is_null($slide->content) && !empty($slide->content)) {
						$inner .= "
					    <div class=\"slide-content\">
					    	{$slide->content}
					    </div>
						";
					}
					
					if (is_page(5)) {
						$inner .= "
						  </div></div>
						";
					} else {
						$inner .= "
						  </div>
						";
					}
				}
			}
			if (!is_null($slide->link) && !empty($slide->link)) {
				$inner .= "
					</a>
				";
			}
			$inner .= "
				</div>
			";
			$slide_count++;
		}
?>
<h2 class="sr-only">Carousel</h2>
<div class="container">
	<div class="row">
		<div id="<?=$carousel->id?>" class="col-12 carousel <?=$carousel->animation?>" data-ride="carousel" data-interval="<?=$carousel->delay?>" style="background-color:<?=$carousel->bgcolour?>">
			<div class="pre-load"><?=$preload?></div>
			<?php
				// Indicators..
				if (is_array($carousel->slides) && count($carousel->slides) > 1) {
					echo "
				<ul class=\"carousel-indicators indicator-{$carousel->indicator}\">
					{$indicators}
				</ul>
					";
				}
				// Slides
			?>
			<div class="carousel-inner" data-height="<?=$carousel->height?>" style="height:<?=$carousel->height?>px;">
				<?=$inner?>
			</div>
			<?php
				// Left / Right Arrows
				if (is_array($carousel->slides) && count($carousel->slides) > 1 && $carousel->navigation) {
			?>
			<a class="carousel-control-prev p4-caret-left-light" href="#<?=$carousel->id?>" data-slide="prev"></a>
			<a class="carousel-control-next p4-caret-right-light" href="#<?=$carousel->id?>" data-slide="next"></a>
	<?php
		}
		echo "
			</div>
		</div>
	</div>
	";
	}
	?>

<script>
	// CAROUSEL
	var carousel_full_screen = false;
</script>
