			<section class="content-panel <?=$panel_class?>">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1><?=$panel_heading?></h1>
							<?php
								if (!is_null($panel_intro) && !empty($panel_intro)) {
									echo "
										<div class=\"intro\">
											{$panel_intro}
										</div>
									";
								}
								if (!is_null($panel_content) && !empty($panel_content)) {
									echo "{$panel_content}";
								}
								if (!is_null($panel_button) && !empty($panel_button)) {
									if (is_null($panel_button_class)) {
										$panel_button_class = "btn-secondary";
									}
									echo "<p><a href=\"{$panel_link}\" target=\"_blank\" class=\"btn {$panel_button_class}\">{$panel_button}</a></p>";
								}
								//$panel_button
							?>
						</div>
					</div>
				</div>
			</section>