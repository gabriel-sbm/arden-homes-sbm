<?php
	// Header
	get_header();

	if (have_posts()) {
?>
		<div class="content blog-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="sr-only">Blog</h1>
<?php
		//
		// CATEGORIES
		//
		// Get Categories
		$args = [
			'type'       => 'post',
			'orderby'    => 'name',
			'order'      => 'ASC', 
			'hide_empty' => 0
		];
		$categories = [];
		foreach (get_categories($args) as $category) {
			if (preg_match('/^Blog/', $category->name)) {
				$categories[] = $category;
			}
			if ($category->slug == 'uncategorised') $categories[] = $category;
		}
		wp_reset_query();

		// Output
		echo "
					</div>
				</div>
			</div>
		</div>
		<div class=\"content blog-filter\">
			<div class=\"container\">
				<div class=\"filter-block\">
					<h2>Categories</h2>
					<ul>
		";
		$filter = (isset($_GET['filter']))?$_GET['filter']:"";
		$output = "";
		$uncat = 0; // This is for 'blog' and 'uncategorised'
		foreach ($categories as $category) {
			if ($category->slug == 'blog' || $category->slug == 'uncategorised') {
				$uncat += $category->category_count;
			} else {
				$output .= "<li".(($category->slug == $filter)?" class=\"selected\"":"")."><a href=\"\blog?filter={$category->slug}\">
				".preg_replace('/Blog\s+-\s+/', '', $category->name)." 
				({$category->category_count})</a></li>";
			}
		}
		if ($uncat > 0) {
			echo "<li><a href=\"\blog\">All Blogs ({$uncat})</a></li>";
		}
		echo "    		
				{$output}		
					</ul>
				</div>
			</div>
		</div>
		";


		//
		// POSTS
		//
		echo "
		<div class=\"content blog-layout\">
			<div class=\"container\">
				<div class=\"row\">
					<div class=\"col-12\">
		";

		// Filtered results
		wp_reset_query();
		$posts_per_page = 6;
		$current_page = (get_query_var( 'paged' ) > 0) ? get_query_var( 'paged' )-1 : 0;
		$args = [
			'post_type' => 'post',
			'posts_per_page' => $posts_per_page,
      'paged' => 1,
      'offset' => $current_page * $posts_per_page,
      'order' => 'DESC'
		];

		if (!empty($filter)) {
			$args['category_name'] = $filter;
		}
		$loop = new WP_Query($args);

		while ( $loop->have_posts() ) {
			$loop->the_post();
			$img = get_the_post_thumbnail_url();
			echo "
				<div class=\"row blog-item\">
					<div class=\"col-12 col-lg-4 blog-image\">
						<img src=\"{$img}?image={$index}\" alt=\"\">
					</div>
					<div class=\"col-12 col-lg-8 blog-content\">
						<h2>".get_the_title()."</h2>
						<p class=\"author\">".get_the_author()." - ".get_the_date()."</p>
						".get_the_excerpt()."
						<p><a class=\"btn btn-secondary btn-continue\" href=\"".get_the_permalink()."\">Continue reading &gt;</a></p>
						<p>
							<a class=\"btn btn-secondary btn-social\" target=\"_blank\" href=\"http://www.facebook.com/sharer.php?u=".get_the_permalink()."\"><span class=\"p4-facebook\"></span>Share</a>
							<a class=\"btn btn-secondary btn-social\" target=\"_blank\" href=\"https://twitter.com/intent/tweet?text=A%20United%20Front%20with%20Colorbond%20&url=".get_the_permalink()."\"><span class=\"p4-twitter\"></span>Tweet</a>
						</p>
					</div>
				</div>
			";
		}
		wp_reset_query();

		echo "
				<nav class=\"navigation pagination\" role=\"navigation\">
			    " .paginate_links([
				    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				    'total'        => $loop->max_num_pages,
				    'current'      => max( 1, get_query_var( 'paged' ) ),
				    'format'       => '?paged=%#%',
				    'show_all'     => false,
				    'type'         => 'plain',
				    'end_size'     => 2,
				    'mid_size'     => 1,
				    'prev_next'    => true,
				    'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
				    'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
				    'add_args'     => false,
				    'add_fragment' => '',
			    ]). "
				</nav>
    ";
?>
					</div>
				</div>
			</div>
		</div>
<?php
	}
?>

	</div>
<?php
	// Footer
	get_footer();
?>