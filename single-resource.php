<?php
  // Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPage();
	include('module/carousel/carousel.php');

	if (have_posts()) {
		while (have_posts()) {
			the_post();
?>
		<div class="content blog-post">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php
							the_content();

						?>
					</div>
				</div>
			</div>
		</div>
	<?php
		}
	}
	?>

	</div>

<?php get_footer(); ?>
