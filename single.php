<?php
  // Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPage();
	include('module/carousel/carousel.php');

	if (have_posts()) {
		while (have_posts()) {
			the_post();
?>
		<div class="content blog-post">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p><?=get_the_author()?> - <?=get_the_date()?></p>

						<?php
							the_content();

						?>

						<p>
							<a class="btn btn-secondary btn-social" target="_blank" href="http://www.facebook.com/sharer.php?u=<?=get_the_permalink()?>"><span class="p4-facebook"></span>Share</a>
							<a class="btn btn-secondary btn-social" target="_blank" href="https://twitter.com/intent/tweet?text=A%20United%20Front%20with%20Colorbond%20&url=<?=get_the_permalink()?>"><span class="p4-twitter"></span>Tweet</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	<?php
		}
	}
	?>

	</div>

<?php get_footer(); ?>
