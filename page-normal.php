<?php
	/* Template Name: Page - Normal Fonts */

	// Header
	get_header();

	if (have_posts()) {
		while (have_posts()) {
			the_post();

			$image = get_field('default_hero_image');
			if (!empty($image)) {
				$slide = Slide::FromPostWithPre("default_hero");
				// CAROUSEL
				$carousel = new Carousel();
				$carousel->slides[] = $slide;
				$carousel->captions = true;
				include('module/carousel/carousel.php');
			}
			// Get Page Content
			$hide_h1 = get_field('default_hide_h1');
			$intro   = get_field('default_introduction');
			$content = get_field('default_content');
			// If no content, hide
			if (!$hide_h1 || (!is_null($intro) && !empty($intro)) || (!is_null($content) && !empty($content))) {
	?>
		<div class="content default-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php 
							// Title
							if (!$hide_h1) {
								echo "<h1>".get_the_title()."</h1>";
							}
							// Intro
							if (!is_null($intro) && !empty($intro)) {
								echo "
									<div class=\"intro\">
										{$intro}
									</div>
								";
							}
							// Content
							if (!is_null($content) && !empty($content)) {
								echo $content;
							}
						?>
					</div>
				</div>
			</div>
		</div>
	<?php
			}
		}
	}
	?>
	</div>
<?php
	// Footer
	get_footer();
?>