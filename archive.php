<?php
	// Header
	get_header();

	if (have_posts()) {
?>
		<div class="content blog-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="sr-only">Blog</h1>
<?php
		while (have_posts()) {
			the_post();
			$img = get_the_post_thumbnail_url();

			echo "
				<div class=\"row blog-item\">
					<div class=\"col-12 col-lg-4 blog-image\">
						<img src=\"{$img}?image={$index}\" alt=\"\">
					</div>
					<div class=\"col-12 col-lg-8 blog-content\">
						<h2>".get_the_title()."</h2>
						<p class=\"author\">".get_the_author()." - ".get_the_date()."</p>
						".get_the_excerpt()."
						<p><a class=\"btn btn-secondary btn-continue\" href=\"".get_the_permalink()."\">Continue reading &gt;</a></p>
						<p>
							<a class=\"btn btn-secondary btn-social\" target=\"_blank\" href=\"http://www.facebook.com/sharer.php?u=".get_the_permalink()."\"><span class=\"p4-facebook\"></span>Share</a>
							<a class=\"btn btn-secondary btn-social\" target=\"_blank\" href=\"https://twitter.com/intent/tweet?text=A%20United%20Front%20with%20Colorbond%20&url=".get_the_permalink()."\"><span class=\"p4-twitter\"></span>Tweet</a>
						</p>
					</div>
				</div>
			";
		}

		the_posts_pagination([
			'prev_text' => __( 'Newer Posts', 'textdomain' ),
		  'next_text' => __( 'Older Posts', 'textdomain' ),
		]);
?>
					</div>
				</div>
			</div>
		</div>
<?php
	}
?>

	</div>
<?php
	// Footer
	get_footer();
?>