<?php
	/* Template Name: Page - Promotions */

	// Header
	get_header();

	// CAROUSEL
	$carousel = Carousel::FromPageWithPre('ph-carousel-panel', 'slide');
	$carousel->navigation = 1;
	$carousel->indicator = 'none';
	include('module/carousel/carousel.php');

	$promotion_content = get_field('promotion_content');

	echo"
		<div class=\"promotions-content\">
			<div class=\"container\">
	";
	if (!empty($promotion_content)) {
		echo $promotion_content;
	}
	echo "
			</div>
		</div>
	</div>
	";				

	// PROMOTIONS
	$features =	FeatureItem::FromRepeaterObject(get_field('promotions'), 'promo');
	FeatureItem::OutputFeaturesWithId('promotions',$features);

	// Footer
	get_footer();

?>